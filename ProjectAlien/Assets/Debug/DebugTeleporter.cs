﻿using UnityEngine;

public class DebugTeleporter : MonoBehaviour
{
    [SerializeField] Transform testRoom;
    private void OnMouseDown()
    {
        GameObject player = GameObject.Find("Player").gameObject;
        player.transform.position = testRoom.position;
    }
}
