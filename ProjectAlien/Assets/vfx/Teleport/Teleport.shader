// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "AndyFX/Teleport"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_Teleport("Teleport", Range( -20 , 20)) = 0
		_Range("Range", Range( -20 , 20)) = 0
		[HDR]_GlowColour("GlowColour", Color) = (0.5313698,2.619777,2.379227,0)
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_TextureSample2("Texture Sample 2", 2D) = "white" {}
		_TextureSample3("Texture Sample 3", 2D) = "bump" {}
		_NoiseScale("NoiseScale", Float) = 5
		_VerOffset_Strength("VerOffset_Strength", Float) = 0
		_Thickness("Thickness", Float) = 0
		_Speed("Speed", Float) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
		};

		uniform float _Teleport;
		uniform float _Range;
		uniform float _VerOffset_Strength;
		uniform float _NoiseScale;
		uniform float _Speed;
		uniform float _Thickness;
		uniform sampler2D _TextureSample3;
		uniform float4 _TextureSample3_ST;
		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;
		uniform float4 _GlowColour;
		uniform sampler2D _TextureSample2;
		uniform float4 _TextureSample2_ST;
		uniform float _Cutoff = 0.5;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 ase_vertex3Pos = v.vertex.xyz;
			float4 transform20 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float YGradient18 = saturate( ( ( transform20.y + _Teleport ) / _Range ) );
			float2 temp_cast_1 = (_NoiseScale).xx;
			float2 panner63 = ( ( _Time.y * _Speed ) * float2( 0,-1 ) + float2( 0,0 ));
			float2 uv_TexCoord1 = v.texcoord.xy * temp_cast_1 + panner63;
			float Noise11 = step( frac( uv_TexCoord1.y ) , _Thickness );
			float3 VertOffset52 = ( ( ( ase_vertex3Pos * YGradient18 ) * _VerOffset_Strength ) * Noise11 );
			v.vertex.xyz += VertOffset52;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_TextureSample3 = i.uv_texcoord * _TextureSample3_ST.xy + _TextureSample3_ST.zw;
			o.Normal = UnpackNormal( tex2D( _TextureSample3, uv_TextureSample3 ) );
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			o.Albedo = tex2D( _TextureSample0, uv_TextureSample0 ).rgb;
			float2 temp_cast_1 = (_NoiseScale).xx;
			float2 panner63 = ( ( _Time.y * _Speed ) * float2( 0,-1 ) + float2( 0,0 ));
			float2 uv_TexCoord1 = i.uv_texcoord * temp_cast_1 + panner63;
			float Noise11 = step( frac( uv_TexCoord1.y ) , _Thickness );
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float4 transform20 = mul(unity_ObjectToWorld,float4( ase_vertex3Pos , 0.0 ));
			float YGradient18 = saturate( ( ( transform20.y + _Teleport ) / _Range ) );
			float4 Emission39 = ( _GlowColour * ( Noise11 * YGradient18 ) );
			o.Emission = Emission39.rgb;
			float2 uv_TextureSample2 = i.uv_texcoord * _TextureSample2_ST.xy + _TextureSample2_ST.zw;
			float4 tex2DNode46 = tex2D( _TextureSample2, uv_TextureSample2 );
			o.Metallic = tex2DNode46.r;
			o.Smoothness = tex2DNode46.r;
			o.Alpha = 1;
			float temp_output_31_0 = ( YGradient18 * 1.0 );
			float OpacityMask28 = ( ( ( ( 1.0 - YGradient18 ) * Noise11 ) - temp_output_31_0 ) + ( 1.0 - temp_output_31_0 ) );
			clip( OpacityMask28 - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17000
293;73;1233;450;5346.3;2026.285;2.550818;True;True
Node;AmplifyShaderEditor.CommentaryNode;24;-4711.214,-855.1035;Float;False;1567.97;601.1625;Ygradient;8;15;17;20;22;16;21;23;18;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;14;-4687.152,-1610.891;Float;False;2036.175;649.5217;Comment;10;1;11;59;60;61;62;63;64;65;66;Noise;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleTimeNode;64;-4448.22,-1321.514;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;66;-4416.941,-1193.489;Float;False;Property;_Speed;Speed;10;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;15;-4661.214,-801.0711;Float;True;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ObjectToWorldTransfNode;20;-4422.494,-805.1035;Float;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;17;-4568.684,-540.8599;Float;True;Property;_Teleport;Teleport;1;0;Create;True;0;0;False;0;0;0;-20;20;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;65;-4234.179,-1271.652;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;63;-4024.489,-1301.971;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,-1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;16;-4135.688,-748.9741;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;22;-4115.85,-511.9407;Float;True;Property;_Range;Range;2;0;Create;True;0;0;False;0;0;-10;-20;20;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;60;-4017.667,-1540.479;Float;True;Property;_NoiseScale;NoiseScale;7;0;Create;True;0;0;False;0;5;5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;1;-3756.066,-1548.055;Float;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleDivideOpNode;21;-3848.337,-730.2343;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;62;-3459.483,-1303.96;Float;False;Property;_Thickness;Thickness;9;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;23;-3572.261,-683.1514;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FractNode;59;-3484.97,-1545.101;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;18;-3386.249,-725.3884;Float;False;YGradient;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;61;-3190.904,-1530.759;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;35;-3129.809,-874.8765;Float;False;1471.241;727.7189;OpacityMask;10;19;25;27;30;26;31;32;33;34;28;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;11;-2893.981,-1559.538;Float;True;Noise;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;19;-3079.809,-824.8765;Float;False;18;YGradient;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;58;-4713.699,-209.7644;Float;False;1359.312;478.2524;VerOffset;8;49;50;51;55;57;54;56;52;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;27;-2886.141,-705.7994;Float;True;11;Noise;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;30;-3069.606,-430.5749;Float;True;18;YGradient;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;25;-2868.827,-820.8633;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;50;-4658.643,23.80005;Float;True;18;YGradient;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;49;-4663.699,-159.7644;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;43;-2414.903,-1772.57;Float;False;1105.44;490.3361;Emission;6;36;37;38;41;42;39;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;31;-2800.832,-440.4381;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;51;-4395.129,-58.92952;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-2605.664,-822.5468;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;36;-2359.031,-1575.582;Float;False;11;Noise;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;37;-2364.903,-1476.333;Float;False;18;YGradient;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;55;-4344.251,153.488;Float;False;Property;_VerOffset_Strength;VerOffset_Strength;8;0;Create;True;0;0;False;0;0;3.55;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;32;-2356.984,-630.3063;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;33;-2487.838,-400.1576;Float;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;38;-2114.578,-1535.234;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;57;-3949.155,131.7686;Float;False;11;Noise;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;54;-4103.254,-50.37241;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;41;-2111.67,-1722.57;Float;False;Property;_GlowColour;GlowColour;3;1;[HDR];Create;True;0;0;False;0;0.5313698,2.619777,2.379227,0;0.5233223,2.109643,3.12358,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;42;-1786.468,-1534.467;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;56;-3823.389,-54.33282;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;34;-2128.33,-441.985;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;28;-1901.568,-632.4985;Float;True;OpacityMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;52;-3597.387,-58.27083;Float;False;VertOffset;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;39;-1552.462,-1535.915;Float;False;Emission;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;29;-283.0109,280.2582;Float;True;28;OpacityMask;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;44;-861.8799,-24.69917;Float;True;Property;_TextureSample0;Texture Sample 0;4;0;Create;True;0;0;False;0;None;d0ad62cf8f388e24b8c1777b10bc26e1;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;47;-861.5829,345.3103;Float;True;Property;_TextureSample3;Texture Sample 3;6;0;Create;True;0;0;False;0;None;c69cbe252cd3ea745aef49964870d6a5;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;13;-474.8888,40.71093;Float;True;39;Emission;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;53;-281.118,480.7886;Float;True;52;VertOffset;1;0;OBJECT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;46;-861.8403,161.7173;Float;True;Property;_TextureSample2;Texture Sample 2;5;0;Create;True;0;0;False;0;None;7bf218b1cc237f9469e04317d9c1c0e1;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;AndyFX/Teleport;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Transparent;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;20;0;15;0
WireConnection;65;0;64;0
WireConnection;65;1;66;0
WireConnection;63;1;65;0
WireConnection;16;0;20;2
WireConnection;16;1;17;0
WireConnection;1;0;60;0
WireConnection;1;1;63;0
WireConnection;21;0;16;0
WireConnection;21;1;22;0
WireConnection;23;0;21;0
WireConnection;59;0;1;2
WireConnection;18;0;23;0
WireConnection;61;0;59;0
WireConnection;61;1;62;0
WireConnection;11;0;61;0
WireConnection;25;0;19;0
WireConnection;31;0;30;0
WireConnection;51;0;49;0
WireConnection;51;1;50;0
WireConnection;26;0;25;0
WireConnection;26;1;27;0
WireConnection;32;0;26;0
WireConnection;32;1;31;0
WireConnection;33;0;31;0
WireConnection;38;0;36;0
WireConnection;38;1;37;0
WireConnection;54;0;51;0
WireConnection;54;1;55;0
WireConnection;42;0;41;0
WireConnection;42;1;38;0
WireConnection;56;0;54;0
WireConnection;56;1;57;0
WireConnection;34;0;32;0
WireConnection;34;1;33;0
WireConnection;28;0;34;0
WireConnection;52;0;56;0
WireConnection;39;0;42;0
WireConnection;0;0;44;0
WireConnection;0;1;47;0
WireConnection;0;2;13;0
WireConnection;0;3;46;0
WireConnection;0;4;46;0
WireConnection;0;10;29;0
WireConnection;0;11;53;0
ASEEND*/
//CHKSM=37068A3C61011B23C8E00EF1E8A430AD831DCE59