// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "AndyFX/BossDisolve"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_Mask("Mask", 2D) = "white" {}
		_Distortion("Distortion", 2D) = "bump" {}
		_DistortionAmount("DistortionAmount", Range( 0 , 1)) = 1
		_ScrollSpeed("ScrollSpeed", Range( 0 , 1)) = 0
		_Color0("Color 0", Color) = (1,0.831139,0,0)
		_Color1("Color 1", Color) = (1,0.1535171,0,0)
		_Power("Power", Range( 0.01 , 5)) = 2
		_Albedo("Albedo", 2D) = "white" {}
		_Burn("Burn", Range( 0 , 1.1)) = 1
		_Normal("Normal", 2D) = "bump" {}
		_MetalicSmoothness("MetalicSmoothness", 2D) = "white" {}
		_HeatWave("HeatWave", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform float4 _Color0;
		uniform float4 _Color1;
		uniform sampler2D _Mask;
		uniform sampler2D _Distortion;
		uniform float4 _Distortion_ST;
		uniform float _DistortionAmount;
		uniform float _ScrollSpeed;
		uniform float _Power;
		uniform float _HeatWave;
		uniform float _Burn;
		uniform sampler2D _MetalicSmoothness;
		uniform float4 _MetalicSmoothness_ST;
		uniform float _Cutoff = 0.5;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			o.Normal = UnpackNormal( tex2D( _Normal, uv_Normal ) );
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			o.Albedo = tex2D( _Albedo, uv_Albedo ).rgb;
			float2 uv_Distortion = i.uv_texcoord * _Distortion_ST.xy + _Distortion_ST.zw;
			float temp_output_15_0 = ( _Time.y * _ScrollSpeed );
			float2 panner11 = ( temp_output_15_0 * float2( 0,1 ) + float2( 0,0 ));
			float2 uv_TexCoord9 = i.uv_texcoord + panner11;
			float4 lerpResult18 = lerp( _Color0 , _Color1 , tex2D( _Mask, ( ( (UnpackNormal( tex2D( _Distortion, uv_Distortion ) )).xyz * _DistortionAmount ) + float3( uv_TexCoord9 ,  0.0 ) ).xy ).r);
			float4 temp_cast_3 = (_Power).xxxx;
			float2 panner35 = ( temp_output_15_0 * float2( 0,-1 ) + i.uv_texcoord);
			float4 tex2DNode22 = tex2D( _Mask, ( ( (UnpackNormal( tex2D( _Distortion, panner35 ) )).xy * _HeatWave ) + i.uv_texcoord ) );
			float temp_output_23_0 = step( tex2DNode22.r , _Burn );
			float temp_output_30_0 = ( 0.0 / 1.1 );
			float temp_output_48_0 = step( tex2DNode22.r , ( 1.0 - _Burn ) );
			float temp_output_50_0 = ( temp_output_48_0 - step( tex2DNode22.r , ( 1.0 - temp_output_30_0 ) ) );
			float4 temp_cast_4 = (temp_output_50_0).xxxx;
			float4 temp_cast_5 = (temp_output_50_0).xxxx;
			o.Emission = ( ( ( ( pow( lerpResult18 , temp_cast_3 ) * _Power ) * ( temp_output_23_0 + ( temp_output_23_0 - step( tex2DNode22.r , temp_output_30_0 ) ) ) ) - temp_cast_4 ) - temp_cast_5 ).rgb;
			float2 uv_MetalicSmoothness = i.uv_texcoord * _MetalicSmoothness_ST.xy + _MetalicSmoothness_ST.zw;
			float4 tex2DNode28 = tex2D( _MetalicSmoothness, uv_MetalicSmoothness );
			o.Metallic = tex2DNode28.r;
			o.Smoothness = tex2DNode28.r;
			o.Alpha = temp_output_48_0;
			clip( temp_output_48_0 - _Cutoff );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float4 tSpace0 : TEXCOORD3;
				float4 tSpace1 : TEXCOORD4;
				float4 tSpace2 : TEXCOORD5;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17000
-1906;33;1906;996;2359.346;1669.536;1.762657;True;False
Node;AmplifyShaderEditor.SimpleTimeNode;13;-3689.147,-553.5786;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;14;-3778.079,-325.9902;Float;False;Property;_ScrollSpeed;ScrollSpeed;4;0;Create;True;0;0;False;0;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;15;-3487.079,-384.9902;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;36;-4001.476,93.04772;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexturePropertyNode;5;-3739.532,-812.8288;Float;True;Property;_Distortion;Distortion;2;0;Create;True;0;0;False;0;a77d49acad1169e4c81eab6c82693a46;a77d49acad1169e4c81eab6c82693a46;True;bump;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.PannerNode;35;-3755.974,92.85357;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,-1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;34;-3500.618,91.31613;Float;True;Property;_TextureSample6;Texture Sample 6;12;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;4;-3433.264,-722.1577;Float;True;Property;_TextureSample2;Texture Sample 2;3;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;6;-3129.324,-722.1801;Float;False;True;True;True;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.PannerNode;11;-3274.08,-386.9879;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ComponentMaskNode;37;-3160.761,91.3113;Float;False;True;True;False;True;1;0;FLOAT3;0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;39;-3159.652,187.5309;Float;False;Property;_HeatWave;HeatWave;12;0;Create;True;0;0;False;0;0;0.001;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;8;-3081.509,-610.7814;Float;False;Property;_DistortionAmount;DistortionAmount;3;0;Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;7;-2903.946,-716.9532;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;9;-3009.764,-382.7048;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;38;-2862.645,98.31576;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;40;-2953.203,270.1517;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexturePropertyNode;1;-2596.284,-922.7847;Float;True;Property;_Mask;Mask;1;0;Create;True;0;0;False;0;63dca6a8e8cbd6e41aab6f08f7744281;63dca6a8e8cbd6e41aab6f08f7744281;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.RangedFloatNode;31;-2300.9,-119.0555;Float;False;Constant;_DivideAmount;DivideAmount;12;0;Create;True;0;0;False;0;1.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;10;-2701.356,-622.4233;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT2;0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;41;-2607.712,152.6203;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;30;-2052.283,-187.3225;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;2;-2316.821,-923.7011;Float;True;Property;_TextureSample0;Texture Sample 0;2;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;16;-1167.12,-1035.061;Float;False;Property;_Color0;Color 0;5;0;Create;True;0;0;False;0;1,0.831139,0,0;1,0.6544965,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;22;-2384.639,-425.9389;Float;True;Property;_TextureSample1;Texture Sample 1;8;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;24;-2416.076,-224.611;Float;False;Property;_Burn;Burn;9;0;Create;True;0;0;False;0;1;0.5;0;1.1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;17;-1164.507,-849.5061;Float;False;Property;_Color1;Color 1;6;0;Create;True;0;0;False;0;1,0.1535171,0,0;1,0,0.05117314,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StepOpNode;29;-1878.238,-288.2514;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-1099.858,-665.3992;Float;False;Property;_Power;Power;7;0;Create;True;0;0;False;0;2;2;0.01;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;23;-2047.911,-393.55;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;18;-827.3713,-935.7499;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;20;-634.7579,-935.4504;Float;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;32;-1723.956,-389.4123;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;42;-1918.326,0.4660339;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;43;-1914.271,220.4563;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;48;-1656.749,-18.88324;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;-419.9451,-932.2819;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StepOpNode;49;-1654.149,209.9168;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;33;-1553.431,-504.8852;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;-1265.826,-543.8717;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;50;-1361.775,78.61713;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;57;-1049.712,-115.0975;Float;True;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;27;-688.3286,-147.5823;Float;True;Property;_Normal;Normal;10;0;Create;True;0;0;False;0;None;eb3e324d147d3d34aa4f8b96aa6ea1ea;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;58;-640.1461,277.3717;Float;True;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;26;-698.2064,-341.5825;Float;True;Property;_Albedo;Albedo;8;0;Create;True;0;0;False;0;None;2c9682149ab742d4d8a3bff6642526b2;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;28;-685.0745,39.5652;Float;True;Property;_MetalicSmoothness;MetalicSmoothness;11;0;Create;True;0;0;False;0;None;97891ec91f31f12409de947cf592f5c9;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;AndyFX/BossDisolve;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Transparent;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;15;0;13;0
WireConnection;15;1;14;0
WireConnection;35;0;36;0
WireConnection;35;1;15;0
WireConnection;34;0;5;0
WireConnection;34;1;35;0
WireConnection;4;0;5;0
WireConnection;6;0;4;0
WireConnection;11;1;15;0
WireConnection;37;0;34;0
WireConnection;7;0;6;0
WireConnection;7;1;8;0
WireConnection;9;1;11;0
WireConnection;38;0;37;0
WireConnection;38;1;39;0
WireConnection;10;0;7;0
WireConnection;10;1;9;0
WireConnection;41;0;38;0
WireConnection;41;1;40;0
WireConnection;30;1;31;0
WireConnection;2;0;1;0
WireConnection;2;1;10;0
WireConnection;22;0;1;0
WireConnection;22;1;41;0
WireConnection;29;0;22;1
WireConnection;29;1;30;0
WireConnection;23;0;22;1
WireConnection;23;1;24;0
WireConnection;18;0;16;0
WireConnection;18;1;17;0
WireConnection;18;2;2;1
WireConnection;20;0;18;0
WireConnection;20;1;19;0
WireConnection;32;0;23;0
WireConnection;32;1;29;0
WireConnection;42;0;24;0
WireConnection;43;0;30;0
WireConnection;48;0;22;1
WireConnection;48;1;42;0
WireConnection;21;0;20;0
WireConnection;21;1;19;0
WireConnection;49;0;22;1
WireConnection;49;1;43;0
WireConnection;33;0;23;0
WireConnection;33;1;32;0
WireConnection;25;0;21;0
WireConnection;25;1;33;0
WireConnection;50;0;48;0
WireConnection;50;1;49;0
WireConnection;57;0;25;0
WireConnection;57;1;50;0
WireConnection;58;0;57;0
WireConnection;58;1;50;0
WireConnection;0;0;26;0
WireConnection;0;1;27;0
WireConnection;0;2;58;0
WireConnection;0;3;28;0
WireConnection;0;4;28;0
WireConnection;0;9;48;0
WireConnection;0;10;48;0
ASEEND*/
//CHKSM=ED146E96555A6134D605E2962A7C08971394CD3A