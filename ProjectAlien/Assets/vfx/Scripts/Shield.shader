// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "AndyFX/Shield"
{
	Properties
	{
		_Albedo("Albedo", 2D) = "white" {}
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_Emissive("Emissive", 2D) = "white" {}
		_Normal("Normal", 2D) = "bump" {}
		_MetallicSmoothness("MetallicSmoothness", 2D) = "white" {}
		_EmissiveShieldMask("EmissiveShieldMask", 2D) = "white" {}
		[HDR]_Emission("Emission", Color) = (0,0,0,0)
		_EmissionMultipler("EmissionMultipler", Range( 0 , 1)) = 0
		_Intensity("Intensity", Float) = 0
		_ShieldStrength("ShieldStrength", Range( 0 , 1)) = 0
		_speedDirectionY("speedDirectionY", Vector) = (0,0,0,0)
		_FresnelStrength("FresnelStrength", Range( 0 , 100)) = 100
		_DynamicExpansion("Dynamic Expansion", Range( 0 , 0.1)) = 0
		_speedDirectionX("speedDirectionX", Vector) = (0,0,0,0)
		_OverallExpansion("Overall Expansion", Range( 0 , 0.5)) = 0
		_ScrollSpeedY("ScrollSpeedY", Float) = 5
		_ScrollSpeedX("ScrollSpeedX", Float) = 5
		[HideInInspector] _texcoord2( "", 2D ) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Off
		AlphaToMask On
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float4 screenPosition;
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
			float2 uv2_texcoord2;
		};

		uniform float _ScrollSpeedX;
		uniform float2 _speedDirectionX;
		uniform float _ScrollSpeedY;
		uniform float2 _speedDirectionY;
		uniform float _ShieldStrength;
		uniform float _DynamicExpansion;
		uniform float _OverallExpansion;
		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform sampler2D _EmissiveShieldMask;
		uniform float4 _EmissiveShieldMask_ST;
		uniform float4 _Emission;
		uniform float _Intensity;
		uniform sampler2D _Emissive;
		uniform float4 _Emissive_ST;
		uniform float _EmissionMultipler;
		uniform sampler2D _MetallicSmoothness;
		uniform float4 _MetallicSmoothness_ST;
		uniform float _FresnelStrength;
		uniform float _Cutoff = 0.5;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		inline float Dither4x4Bayer( int x, int y )
		{
			const float dither[ 16 ] = {
				 1,  9,  3, 11,
				13,  5, 15,  7,
				 4, 12,  2, 10,
				16,  8, 14,  6 };
			int r = y * 4 + x;
			return dither[r] / 16; // same # of instructions as pre-dividing due to compiler magic
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float2 temp_cast_0 = (_speedDirectionX.x).xx;
			float2 panner18 = ( ( _Time.y * _ScrollSpeedX ) * temp_cast_0 + float2( 0,0 ));
			float2 uv2_TexCoord19 = v.texcoord1.xy * float2( 3,3 ) + panner18;
			float simplePerlin2D20 = snoise( uv2_TexCoord19 );
			simplePerlin2D20 = simplePerlin2D20*0.5 + 0.5;
			float2 temp_cast_1 = (_speedDirectionY.y).xx;
			float2 panner22 = ( ( _Time.y * _ScrollSpeedY ) * temp_cast_1 + float2( 0,0 ));
			float2 uv2_TexCoord23 = v.texcoord1.xy * float2( 3,3 ) + panner22;
			float simplePerlin2D21 = snoise( uv2_TexCoord23 );
			simplePerlin2D21 = simplePerlin2D21*0.5 + 0.5;
			float temp_output_25_0 = ( ( simplePerlin2D20 + simplePerlin2D21 ) * _ShieldStrength );
			float3 ase_vertexNormal = v.normal.xyz;
			v.vertex.xyz += ( ( ( temp_output_25_0 * ase_vertexNormal ) * _DynamicExpansion ) + ( _OverallExpansion * ase_vertexNormal ) );
			float4 ase_screenPos = ComputeScreenPos( UnityObjectToClipPos( v.vertex ) );
			o.screenPosition = ase_screenPos;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			o.Normal = UnpackNormal( tex2D( _Normal, uv_Normal ) );
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			o.Albedo = tex2D( _Albedo, uv_Albedo ).rgb;
			float2 uv_EmissiveShieldMask = i.uv_texcoord * _EmissiveShieldMask_ST.xy + _EmissiveShieldMask_ST.zw;
			float2 uv_Emissive = i.uv_texcoord * _Emissive_ST.xy + _Emissive_ST.zw;
			float4 temp_output_12_0 = ( tex2D( _Emissive, uv_Emissive ) * _EmissionMultipler );
			o.Emission = ( ( ( tex2D( _EmissiveShieldMask, uv_EmissiveShieldMask ) * _Emission ) * _Intensity ) * temp_output_12_0 ).rgb;
			float2 uv_MetallicSmoothness = i.uv_texcoord * _MetallicSmoothness_ST.xy + _MetallicSmoothness_ST.zw;
			o.Smoothness = tex2D( _MetallicSmoothness, uv_MetallicSmoothness ).r;
			o.Alpha = ( 1.0 - temp_output_12_0 ).r;
			float4 ase_screenPos = i.screenPosition;
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float2 clipScreen27 = ase_screenPosNorm.xy * _ScreenParams.xy;
			float dither27 = Dither4x4Bayer( fmod(clipScreen27.x, 4), fmod(clipScreen27.y, 4) );
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float fresnelNdotV6 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode6 = ( 0.0 + 1.0 * pow( 1.0 - fresnelNdotV6, 5.0 ) );
			float2 temp_cast_4 = (_speedDirectionX.x).xx;
			float2 panner18 = ( ( _Time.y * _ScrollSpeedX ) * temp_cast_4 + float2( 0,0 ));
			float2 uv2_TexCoord19 = i.uv2_texcoord2 * float2( 3,3 ) + panner18;
			float simplePerlin2D20 = snoise( uv2_TexCoord19 );
			simplePerlin2D20 = simplePerlin2D20*0.5 + 0.5;
			float2 temp_cast_5 = (_speedDirectionY.y).xx;
			float2 panner22 = ( ( _Time.y * _ScrollSpeedY ) * temp_cast_5 + float2( 0,0 ));
			float2 uv2_TexCoord23 = i.uv2_texcoord2 * float2( 3,3 ) + panner22;
			float simplePerlin2D21 = snoise( uv2_TexCoord23 );
			simplePerlin2D21 = simplePerlin2D21*0.5 + 0.5;
			float temp_output_25_0 = ( ( simplePerlin2D20 + simplePerlin2D21 ) * _ShieldStrength );
			dither27 = step( dither27, ( ( fresnelNode6 * ( _FresnelStrength * _ShieldStrength ) ) * temp_output_25_0 ) );
			#if UNITY_PASS_SHADOWCASTER
			clip( dither27 - _Cutoff );
			#endif
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard keepalpha fullforwardshadows exclude_path:deferred vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			AlphaToMask Off
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float4 customPack1 : TEXCOORD1;
				float4 customPack2 : TEXCOORD2;
				float4 tSpace0 : TEXCOORD3;
				float4 tSpace1 : TEXCOORD4;
				float4 tSpace2 : TEXCOORD5;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.customPack2.xyzw = customInputData.screenPosition;
				o.customPack1.zw = customInputData.uv2_texcoord2;
				o.customPack1.zw = v.texcoord1;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				surfIN.screenPosition = IN.customPack2.xyzw;
				surfIN.uv2_texcoord2 = IN.customPack1.zw;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17000
0;73;1305;666;2325.418;364.5624;1.071452;True;False
Node;AmplifyShaderEditor.RangedFloatNode;56;-2423.855,761.0593;Float;False;Property;_ScrollSpeedX;ScrollSpeedX;16;0;Create;True;0;0;False;0;5;5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;55;-2415.855,693.0593;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;53;-2410.855,918.0593;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;57;-2416.855,989.0593;Float;False;Property;_ScrollSpeedY;ScrollSpeedY;15;0;Create;True;0;0;False;0;5;5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;32;-2083.716,904.1324;Float;False;Property;_speedDirectionY;speedDirectionY;10;0;Create;True;0;0;False;0;0,0;0,0.11;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.Vector2Node;31;-2076.208,696.2902;Float;False;Property;_speedDirectionX;speedDirectionX;13;0;Create;True;0;0;False;0;0,0;0.1,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;59;-2225.338,919.611;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;58;-2230.338,705.611;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;22;-1896.406,932.1028;Float;True;3;0;FLOAT2;0,0;False;2;FLOAT2;-1,-1;False;1;FLOAT;5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;18;-1890.721,700.7162;Float;True;3;0;FLOAT2;0,0;False;2;FLOAT2;1,-1;False;1;FLOAT;5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;23;-1627.789,884.7503;Float;False;1;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;3,3;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;19;-1621.971,655.0549;Float;False;1;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;3,3;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NoiseGeneratorNode;20;-1373.132,649.9814;Float;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;21;-1374.656,879.8089;Float;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;24;-1015.147,824.2902;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;26;-1116.712,1027.665;Float;False;Property;_ShieldStrength;ShieldStrength;9;0;Create;True;0;0;False;0;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;35;-1200.437,570.5718;Float;False;Property;_FresnelStrength;FresnelStrength;11;0;Create;True;0;0;False;0;100;100;0;100;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;-775.1463,840.2902;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;9;-1794.459,-197.5665;Float;False;Property;_Emission;Emission;6;1;[HDR];Create;True;0;0;False;0;0,0,0,0;0,1.83465,1.566538,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;5;-1594.831,-219.7687;Float;True;Property;_EmissiveShieldMask;EmissiveShieldMask;5;0;Create;True;0;0;False;0;e5220ac66edca3b4e80686105e82f597;e5220ac66edca3b4e80686105e82f597;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FresnelNode;6;-1078.365,395.2098;Float;False;Standard;WorldNormal;ViewDir;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;36;-910.2393,625.9731;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalVertexDataNode;37;-771.0947,1183.876;Float;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;2;-1438.471,139.2933;Float;True;Property;_Emissive;Emissive;2;0;Create;True;0;0;False;0;e2ad3e680a9ae6b449a27062090d6ee0;e2ad3e680a9ae6b449a27062090d6ee0;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;16;-1263.626,-86.1582;Float;False;Property;_Intensity;Intensity;8;0;Create;True;0;0;False;0;0;50;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NormalVertexDataNode;48;-733.097,1678.277;Float;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;45;-808.047,1391.56;Float;False;Property;_DynamicExpansion;Dynamic Expansion;12;0;Create;True;0;0;False;0;0;0;0;0.1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;38;-527.8447,1183.876;Float;True;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;11;-1721.6,161.9914;Float;False;Property;_EmissionMultipler;EmissionMultipler;7;0;Create;True;0;0;False;0;0;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;7;-761.8637,494.0066;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;47;-816.554,1585.546;Float;False;Property;_OverallExpansion;Overall Expansion;14;0;Create;True;0;0;False;0;0;0;0;0.5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;-1249.186,-213.9508;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;46;-294.4225,1372.359;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;12;-1045.505,142.7497;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;17;-1058.024,-213.9072;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;49;-292.6292,1481.998;Float;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;28;-612.8047,494.3037;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;50;-111.8058,1417.087;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DitheringNode;27;-293.6457,487.7305;Float;False;0;False;3;0;FLOAT;0;False;1;SAMPLER2D;;False;2;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;14;-816.8632,45.17162;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;1;-438.4463,-188.1393;Float;True;Property;_Albedo;Albedo;0;0;Create;True;0;0;False;0;d9f53e8439c87c847918c04557f143d8;d9f53e8439c87c847918c04557f143d8;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;3;-409.8702,6.249101;Float;True;Property;_Normal;Normal;3;0;Create;True;0;0;False;0;8528af1e77aea1d42ae9a4783cc97d34;8528af1e77aea1d42ae9a4783cc97d34;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;4;-2168.145,87.94186;Float;True;Property;_MetallicSmoothness;MetallicSmoothness;4;0;Create;True;0;0;False;0;80827d6a3127b074c9b795ea9b969c94;80827d6a3127b074c9b795ea9b969c94;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;13;-831.3267,201.4019;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;58.49998,-11.7;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;AndyFX/Shield;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Transparent;;Geometry;ForwardOnly;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;1;-1;-1;-1;0;True;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;59;0;53;0
WireConnection;59;1;57;0
WireConnection;58;0;55;0
WireConnection;58;1;56;0
WireConnection;22;2;32;2
WireConnection;22;1;59;0
WireConnection;18;2;31;1
WireConnection;18;1;58;0
WireConnection;23;1;22;0
WireConnection;19;1;18;0
WireConnection;20;0;19;0
WireConnection;21;0;23;0
WireConnection;24;0;20;0
WireConnection;24;1;21;0
WireConnection;25;0;24;0
WireConnection;25;1;26;0
WireConnection;36;0;35;0
WireConnection;36;1;26;0
WireConnection;38;0;25;0
WireConnection;38;1;37;0
WireConnection;7;0;6;0
WireConnection;7;1;36;0
WireConnection;10;0;5;0
WireConnection;10;1;9;0
WireConnection;46;0;38;0
WireConnection;46;1;45;0
WireConnection;12;0;2;0
WireConnection;12;1;11;0
WireConnection;17;0;10;0
WireConnection;17;1;16;0
WireConnection;49;0;47;0
WireConnection;49;1;48;0
WireConnection;28;0;7;0
WireConnection;28;1;25;0
WireConnection;50;0;46;0
WireConnection;50;1;49;0
WireConnection;27;0;28;0
WireConnection;14;0;17;0
WireConnection;14;1;12;0
WireConnection;13;0;12;0
WireConnection;0;0;1;0
WireConnection;0;1;3;0
WireConnection;0;2;14;0
WireConnection;0;4;4;0
WireConnection;0;9;13;0
WireConnection;0;10;27;0
WireConnection;0;11;50;0
ASEEND*/
//CHKSM=85EEA53C32D8E962C241301395D669D8C188CBE0