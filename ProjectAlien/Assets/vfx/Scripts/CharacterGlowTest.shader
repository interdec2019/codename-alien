// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "AndyFX/CharacterGlowTest"
{
	Properties
	{
		_Leyssa_lp_default_AlbedoTransparency("Leyssa_lp_default_AlbedoTransparency", 2D) = "white" {}
		_Leyssa_lp_default_Emission("Leyssa_lp_default_Emission", 2D) = "white" {}
		_Leyssa_lp_default_MetallicSmoothness("Leyssa_lp_default_MetallicSmoothness", 2D) = "white" {}
		_Leyssa_lp_default_Normal("Leyssa_lp_default_Normal", 2D) = "bump" {}
		_RimColour("RimColour", Color) = (0,0,0,0)
		[HDR]_FaceShield("FaceShield", Color) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
		};

		uniform sampler2D _Leyssa_lp_default_Normal;
		uniform float4 _Leyssa_lp_default_Normal_ST;
		uniform sampler2D _Leyssa_lp_default_AlbedoTransparency;
		uniform float4 _Leyssa_lp_default_AlbedoTransparency_ST;
		uniform float4 _RimColour;
		uniform sampler2D _Leyssa_lp_default_Emission;
		uniform float4 _Leyssa_lp_default_Emission_ST;
		uniform float4 _FaceShield;
		uniform sampler2D _Leyssa_lp_default_MetallicSmoothness;
		uniform float4 _Leyssa_lp_default_MetallicSmoothness_ST;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Leyssa_lp_default_Normal = i.uv_texcoord * _Leyssa_lp_default_Normal_ST.xy + _Leyssa_lp_default_Normal_ST.zw;
			o.Normal = UnpackNormal( tex2D( _Leyssa_lp_default_Normal, uv_Leyssa_lp_default_Normal ) );
			float2 uv_Leyssa_lp_default_AlbedoTransparency = i.uv_texcoord * _Leyssa_lp_default_AlbedoTransparency_ST.xy + _Leyssa_lp_default_AlbedoTransparency_ST.zw;
			o.Albedo = tex2D( _Leyssa_lp_default_AlbedoTransparency, uv_Leyssa_lp_default_AlbedoTransparency ).rgb;
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float fresnelNdotV8 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode8 = ( 0.0 + 1.0 * pow( 1.0 - fresnelNdotV8, 5.0 ) );
			float2 uv_Leyssa_lp_default_Emission = i.uv_texcoord * _Leyssa_lp_default_Emission_ST.xy + _Leyssa_lp_default_Emission_ST.zw;
			o.Emission = ( ( pow( fresnelNode8 , _RimColour.r ) * _RimColour ) + ( tex2D( _Leyssa_lp_default_Emission, uv_Leyssa_lp_default_Emission ) * _FaceShield ) ).rgb;
			float2 uv_Leyssa_lp_default_MetallicSmoothness = i.uv_texcoord * _Leyssa_lp_default_MetallicSmoothness_ST.xy + _Leyssa_lp_default_MetallicSmoothness_ST.zw;
			float4 tex2DNode6 = tex2D( _Leyssa_lp_default_MetallicSmoothness, uv_Leyssa_lp_default_MetallicSmoothness );
			o.Metallic = tex2DNode6.r;
			o.Smoothness = tex2DNode6.r;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard keepalpha fullforwardshadows exclude_path:deferred 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17000
194;93;1705;912;2079.185;1252.963;2.30623;True;True
Node;AmplifyShaderEditor.ColorNode;10;-748.4933,-869.4277;Float;False;Property;_RimColour;RimColour;5;0;Create;True;0;0;False;0;0,0,0,0;0.2024741,0.3006617,0.4716981,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FresnelNode;8;-771.4933,-1076.428;Float;True;Standard;WorldNormal;ViewDir;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;33;-420.25,-1046.078;Float;False;2;0;FLOAT;0;False;1;COLOR;1,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;5;-1276.325,266.4173;Float;True;Property;_Leyssa_lp_default_Emission;Leyssa_lp_default_Emission;2;0;Create;True;0;0;False;0;05f2370413b4e3840bddbc09f58a8770;05f2370413b4e3840bddbc09f58a8770;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;32;-1181.23,472.7571;Float;False;Property;_FaceShield;FaceShield;6;1;[HDR];Create;True;0;0;False;0;0,0,0,0;42.22425,7.376893,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;-228.4934,-1058.528;Float;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;-739.5578,263.0731;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;4;-1290.373,-328.0439;Float;True;Property;_Leyssa_lp_default_AlbedoTransparency;Leyssa_lp_default_AlbedoTransparency;1;0;Create;True;0;0;False;0;d0ad62cf8f388e24b8c1777b10bc26e1;d0ad62cf8f388e24b8c1777b10bc26e1;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;7;-1273.017,-139.6799;Float;True;Property;_Leyssa_lp_default_Normal;Leyssa_lp_default_Normal;4;0;Create;True;0;0;False;0;c69cbe252cd3ea745aef49964870d6a5;c69cbe252cd3ea745aef49964870d6a5;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;6;-1284.007,63.07678;Float;True;Property;_Leyssa_lp_default_MetallicSmoothness;Leyssa_lp_default_MetallicSmoothness;3;0;Create;True;0;0;False;0;7bf218b1cc237f9469e04317d9c1c0e1;7bf218b1cc237f9469e04317d9c1c0e1;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;24;-326.9323,-322.5124;Float;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;AndyFX/CharacterGlowTest;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Transparent;;Geometry;ForwardOnly;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;33;0;8;0
WireConnection;33;1;10;0
WireConnection;9;0;33;0
WireConnection;9;1;10;0
WireConnection;29;0;5;0
WireConnection;29;1;32;0
WireConnection;24;0;9;0
WireConnection;24;1;29;0
WireConnection;0;0;4;0
WireConnection;0;1;7;0
WireConnection;0;2;24;0
WireConnection;0;3;6;0
WireConnection;0;4;6;0
ASEEND*/
//CHKSM=D671EADF8361E6DB567AF264740252A5CFC4AB68