// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "AndyFX/PowerCoreHolder"
{
	Properties
	{
		_Albedo_0("Albedo_0", 2D) = "white" {}
		_Albedo_1("Albedo_1", 2D) = "white" {}
		[Toggle]_PowerSwitch("PowerSwitch", Range( 0 , 1)) = 0
		_Normal("Normal", 2D) = "bump" {}
		_Emissive("Emissive", 2D) = "white" {}
		[IntRange]_EmissivePower("EmissivePower", Range( 0 , 5)) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows exclude_path:deferred 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform sampler2D _Albedo_0;
		uniform float4 _Albedo_0_ST;
		uniform sampler2D _Albedo_1;
		uniform float4 _Albedo_1_ST;
		uniform float _PowerSwitch;
		uniform sampler2D _Emissive;
		uniform float4 _Emissive_ST;
		uniform float _EmissivePower;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			o.Normal = UnpackNormal( tex2D( _Normal, uv_Normal ) );
			float2 uv_Albedo_0 = i.uv_texcoord * _Albedo_0_ST.xy + _Albedo_0_ST.zw;
			float2 uv_Albedo_1 = i.uv_texcoord * _Albedo_1_ST.xy + _Albedo_1_ST.zw;
			float4 lerpResult4 = lerp( tex2D( _Albedo_0, uv_Albedo_0 ) , tex2D( _Albedo_1, uv_Albedo_1 ) , _PowerSwitch);
			o.Albedo = lerpResult4.rgb;
			float2 uv_Emissive = i.uv_texcoord * _Emissive_ST.xy + _Emissive_ST.zw;
			float4 temp_cast_1 = (_EmissivePower).xxxx;
			float4 Emission12 = ( pow( tex2D( _Emissive, uv_Emissive ) , temp_cast_1 ) * _EmissivePower );
			o.Emission = ( lerpResult4 * Emission12 ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17000
-1913;1;1906;1021;1499.147;971.37;1;True;False
Node;AmplifyShaderEditor.SamplerNode;7;-873.95,-638.1631;Float;True;Property;_Emissive;Emissive;5;0;Create;True;0;0;False;0;f97ce56b3f051ac47b9df4fc5eeeb252;f97ce56b3f051ac47b9df4fc5eeeb252;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;15;-857.9617,-451.1374;Float;False;Property;_EmissivePower;EmissivePower;6;1;[IntRange];Create;True;0;0;False;0;1;1;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;17;-533.147,-631.37;Float;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;14;-232.3624,-634.5372;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;2;-855.0112,98.25381;Float;True;Property;_Albedo_1;Albedo_1;2;0;Create;True;0;0;False;0;23f696c1ba11227439afad14c5fd7f5d;23f696c1ba11227439afad14c5fd7f5d;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;5;-827.0112,-162.7461;Float;False;Property;_PowerSwitch;PowerSwitch;3;1;[Toggle];Create;True;0;0;False;0;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;1;-855.0112,-85.74614;Float;True;Property;_Albedo_0;Albedo_0;1;0;Create;True;0;0;False;0;758d40f649fc3c24587f589a28f6f875;758d40f649fc3c24587f589a28f6f875;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;12;-72.85641,-639.3649;Float;False;Emission;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;4;-519.0114,-1.746131;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;13;-535.9229,136.558;Float;False;12;Emission;1;0;OBJECT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;16;-241.9622,208.0626;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;6;-843.509,281.8349;Float;True;Property;_Normal;Normal;4;0;Create;True;0;0;False;0;803788047b5fa1a4ba974e12288f3b0c;803788047b5fa1a4ba974e12288f3b0c;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;AndyFX/PowerCoreHolder;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Transparent;;Geometry;ForwardOnly;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;17;0;7;0
WireConnection;17;1;15;0
WireConnection;14;0;17;0
WireConnection;14;1;15;0
WireConnection;12;0;14;0
WireConnection;4;0;1;0
WireConnection;4;1;2;0
WireConnection;4;2;5;0
WireConnection;16;0;4;0
WireConnection;16;1;13;0
WireConnection;0;0;4;0
WireConnection;0;1;6;0
WireConnection;0;2;16;0
ASEEND*/
//CHKSM=A7AAC55E330D65C5044C69BF31913D476FBDB7E5