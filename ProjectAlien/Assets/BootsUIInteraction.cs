﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class BootsUIInteraction : MonoBehaviour
{
    [SerializeField] CanvasGroup playerCanvas;
    [SerializeField] Camera actualCamera;
    [SerializeField] CinemachineVirtualCamera main;
    [SerializeField] CinemachineVirtualCamera zoom;
    [SerializeField] LayerMask noPlayer;
    [SerializeField] LayerMask everything;

    [SerializeField] CanvasGroup interactCanvas;
    [SerializeField] CanvasGroup innerCanvas;
    [SerializeField] GameObject eLock;
    [SerializeField] GameObject ePick;

    public bool locked;
    PlayerControl playerMov;

    bool inside;
    bool zoomedIn;

    private void Start()
    {
        playerMov = GameObject.Find("Player").GetComponent<PlayerControl>();
        innerCanvas.alpha = 0f;
    }
    void Update()
    {   if (inside)
        {
            interactCanvas.alpha = 1;
            if (Input.GetButtonDown("Interact"))
            {
                if (!zoomedIn)
                {
                    StartCoroutine(ZoomIn());
                }
                
            }
            else if (Input.GetButtonDown("Cancel"))
            {
                if (zoomedIn)
                {
                    StartCoroutine(ZoomOut());
                }
            }

        }
        else
        {
            interactCanvas.alpha = 0;
        }
        if (locked)
        {
            eLock.SetActive(true);
            ePick.SetActive(false);
        }
        else
        {
            eLock.SetActive(false);
            ePick.SetActive(true);
        }
        
    }
    IEnumerator ZoomIn()
    {
        zoom.Priority = 12;
        playerMov.enabled = false;
        StartCoroutine(HideUIAndPlayer());
        zoomedIn = true;
        
        yield return null;
    }
    IEnumerator ZoomOut()
    {
        zoom.Priority = 0;
        StartCoroutine(ShowUIAndPlayer());
        zoomedIn = false;        
        yield return null;
    }
    IEnumerator HideUIAndPlayer()
    {
        yield return new WaitForSeconds(1.8f);
        actualCamera.cullingMask = noPlayer;
        playerCanvas.alpha = 0f;
        innerCanvas.alpha = 1f;
        yield return null;
    }
    IEnumerator ShowUIAndPlayer()
    {
        innerCanvas.alpha = 0f;
        actualCamera.cullingMask = everything;
        playerCanvas.alpha = 1f;
        yield return new WaitForSeconds(1.8f);
        playerMov.enabled = true;
        yield return null;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            inside = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            inside = false;
        }
    }
}
