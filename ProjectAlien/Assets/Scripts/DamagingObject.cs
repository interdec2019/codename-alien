﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagingObject : MonoBehaviour
{
    [SerializeField] float damageToPlayerUponImpact = 2;
    Component damageable; //player

    private void OnCollisionEnter(Collision collision)//deal damage to player
    {
        //Bruno:- For collison of destructable objects
        if (collision.gameObject.tag == "Player")
        {
            damageable = collision.collider.GetComponent(typeof(IDamageable));
        }
        DealDamage(damageToPlayerUponImpact);
    }

    void DealDamage(float _damage)
    {
        if (damageable)
        {
            (damageable as IDamageable).TakeDamage(_damage);
        }
    }
}
