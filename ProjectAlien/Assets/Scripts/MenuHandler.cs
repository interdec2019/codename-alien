﻿using UnityEngine;
using System.Collections;


public class MenuHandler : MonoBehaviour
{
    [SerializeField] CanvasGroup esc_MenuOverlay;

    [SerializeField] Animator esc_MenuWorldSpaceObject;
    bool escMenuActive;

    [SerializeField] CanvasGroup dead_MenuOverlay;
    [SerializeField] AudioClip tabletOpenfx;
    [SerializeField] AudioClip tabletClosefx;

    public AudioSource tabletfx;
     

    // Start is called before the first frame update
    void Start()
    {
        //DeactivateEscMenu();
        DeactivateDeathMenu();
        tabletfx = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (!escMenuActive)
            {
              
                ActivateEscMenu();                
            }

            else if (escMenuActive)
            {
                
                DeactivateEscMenu();                
            }
        }
    }
    
    public void ActivateEscMenu() // To make better version of code later
    {
        tabletfx.PlayOneShot(tabletOpenfx);
        esc_MenuWorldSpaceObject.SetBool("Activate", true);
      
        Time.timeScale = 0f;
        esc_MenuOverlay.alpha = 1f;
        esc_MenuOverlay.blocksRaycasts = true;
        esc_MenuOverlay.interactable = true;
        escMenuActive = true;
        
    }

    public void DeactivateEscMenu()
    {
        tabletfx.PlayOneShot(tabletClosefx);
        Time.timeScale = 1f;
        esc_MenuWorldSpaceObject.SetBool("Activate", false);
        
        esc_MenuOverlay.blocksRaycasts = false;
        esc_MenuOverlay.interactable = false;
        escMenuActive = false;
    }

    public void ActivateDeathMenu()
    {
        Time.timeScale = 0f;
        dead_MenuOverlay.alpha = 1f;
        dead_MenuOverlay.interactable = true;
        dead_MenuOverlay.blocksRaycasts = true;
        escMenuActive = true;// prevents player from opening ESC menu when dying
    }

    public void DeactivateDeathMenu()
    {
        Time.timeScale = 1f;
        dead_MenuOverlay.alpha = 0f;
        dead_MenuOverlay.interactable = false;
        dead_MenuOverlay.blocksRaycasts = false;
        escMenuActive = false;// prevents player from opening ESC menu when dying
    }

    /*
     * BY: Parth Thakkar 
     * Date: 31/10/2019
     */
    public void TeleportToLastCheckPoint()
    {
        GameManager.instance.LoadPlayerIntoLevel();
        DeactivateDeathMenu();
        DeactivateEscMenu();
    }
}
