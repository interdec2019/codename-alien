﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldProjectile : MonoBehaviour
{
    [SerializeField] GameObject particlesImpact;
    Shield shieldScript;
    
    float damageToEnemy;

    private void Awake()
    {
        shieldScript = GameObject.Find("Player").GetComponent<Shield>();
        
    }
    private void OnTriggerEnter(Collider collider)
    {
        if (shieldScript.currentHealth >= 50f) // TODO Currently not working as intended
        {
            damageToEnemy = 25f;
        }
        else
        {
            damageToEnemy = 15f;
        }
        Component damageable = collider.GetComponent(typeof(IDamageable));
        if (damageable)
        {
            
            (damageable as IDamageable).TakeDamage(damageToEnemy);
        }
        shieldScript.DepletTotalEnergy(0f);
        Destroy(gameObject, 0.4f);
    }
   
}
