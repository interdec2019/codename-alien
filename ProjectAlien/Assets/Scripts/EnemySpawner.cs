﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    EnemyHealthAndDamageSystem[] enemies;
    GameObject enemyHolder;
    bool playerInRoom;
    bool playerLeftRoom;
    bool enemiesRespawned;

    [SerializeField] float timerTillRespawn=0.5f;
    bool enemiesReadyToRespawn=true;

    void Awake()
    {
        enemyHolder = GameObject.Find("EnemyHolder");
        enemies = enemyHolder.GetComponentsInChildren<EnemyHealthAndDamageSystem>();
        enemyHolder.SetActive(true);
        
    }
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            playerInRoom = true;          
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            playerLeftRoom= true;
            
        }
    }
    void RespawnEnemies()
    {
        if (enemiesReadyToRespawn)
        { if (!enemiesRespawned)
            {

            
            enemyHolder.SetActive(true);
            foreach (EnemyHealthAndDamageSystem enemy in enemies)
            {
                enemy.gameObject.SetActive(true);
                enemy.GetComponent<EnemyHealthAndDamageSystem>().Resurect();
            }
            enemiesReadyToRespawn = false;
            enemiesRespawned = true;
            }
        }
    }

    public IEnumerator StartTimerToRespawn()
    {
        yield return new WaitForSeconds(timerTillRespawn);
        enemiesReadyToRespawn = true;
        RespawnEnemies();
        yield return null;
    }

    void Update()
    {
        if (playerLeftRoom)
        {
            playerInRoom = false;            
            StartCoroutine(StartTimerToRespawn());            
        }
        if (playerInRoom)
        {
            playerInRoom = true;
            enemiesReadyToRespawn = false;
            //StopAllCoroutines();
        }
    }
}
