﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerCorePickup : MonoBehaviour
{
    public GameObject generator;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            generator.GetComponent<PowerCorePlacement>().pickedupCore = true;
            Destroy(gameObject);

        }
    }
}
