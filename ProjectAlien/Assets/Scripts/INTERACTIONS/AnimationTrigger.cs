﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationTrigger : MonoBehaviour
{
    [SerializeField] AudioClip LightFallingfx;
    public AudioSource lightFalling;
    public Animator animator;
    public bool repeatableAnimation;
    bool insideTrigger;
    bool played;

   
    void Update()
    {
        if (!repeatableAnimation)
        {
            if (insideTrigger && !played)
            {
                animator.SetTrigger("Activate");
                played = true;
            }
        }

        if (repeatableAnimation)
        {
            animator.SetTrigger("Activate");
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            if (!played)
            {
                animator.SetTrigger("Activate");
                played = true;
            }
        }   
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            insideTrigger = true;
            lightFalling.PlayOneShot(LightFallingfx);
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        insideTrigger = false;
    }

    void Start()
    {
        lightFalling = GetComponent<AudioSource>();
    }
}

