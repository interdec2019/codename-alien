﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformLatchCollision : MonoBehaviour
{
    [SerializeField] AudioClip cableFallingfx;
    public AudioSource cableFalling;
    public Animator animator1;
    public string animationTrigger1;
    bool collided;
    bool unlatched;

    void Update()
    {
        if (collided)
        {
            animator1.SetTrigger(animationTrigger1);
            unlatched = true;
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            gameObject.GetComponent<Collider>().enabled = false;
        }     
    } 
    

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            if (!unlatched)
            {
                collided = true;
                cableFalling.PlayOneShot(cableFallingfx);
            }  
        }
    }

    void Start()
    {
        cableFalling = GetComponent<AudioSource>();
    }
}
