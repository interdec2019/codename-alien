﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushButton : MonoBehaviour
{
    [SerializeField] AudioClip pushButtonfx;
    public AudioSource pushButton;
    public bool insideTrigger;
    public bool pressedButton;

    public Material green;

    public GameObject door;
    public GameObject button;
    public GameObject wire;

   // public string doorName;

    private void Start()
    {
        // = GameObject.Find(doorName);
        pushButton = GetComponent<AudioSource>();

    }

    private void Update()
    {
        if (insideTrigger)
        {
            PressButton();
        }
    }

    private void OnTriggerEnter(Collider other)
    {       
        if (other.gameObject.tag == "Player")
        {
            insideTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        insideTrigger = false;
    }

    void PressButton()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (!pressedButton)
            {
                pressedButton = true;
                button.GetComponent<MeshRenderer>().material = green;
                wire.GetComponent<MeshRenderer>().material = green;
                door.GetComponent<DoorOpeningClosing>().OpenDoor();
                door.GetComponent<DoorOpeningClosing>().isLocked = false;
                door.GetComponent<BoxCollider>().enabled = false;
                pushButton.PlayOneShot(pushButtonfx);
            }
        }       
    }
}
