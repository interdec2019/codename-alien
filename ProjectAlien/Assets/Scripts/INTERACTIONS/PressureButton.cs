﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressureButton : MonoBehaviour
{
    public AudioSource DoorAudioSource;
    [SerializeField] AudioClip doorOpenfx;
    [SerializeField] AudioClip doorClosefx;
    bool insideTrigger;
    public GameObject door;
    public GameObject crate;

    DoorOpeningClosing doorScript;


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "Crate")
        {
            InsideTrigger();
            print("inside booboo");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "Crate")
        {
            OutsideTrigger();
        }
    }

    void InsideTrigger()
    {
        //crate.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX;
        crate.GetComponent<Rigidbody>().constraints =  RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
        //crate.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY;
       // crate.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;
        door.GetComponent<DoorOpeningClosing>().OpenDoor();
        door.GetComponent<DoorOpeningClosing>().isPressureDoor = false;
        door.GetComponent<BoxCollider>().enabled = false;
        DoorAudioSource.PlayOneShot(doorOpenfx);
        print("AHHHHHH");
        
    }

    void OutsideTrigger()
    {
        door.GetComponent<DoorOpeningClosing>().CloseDoor();
        door.GetComponent<DoorOpeningClosing>().isPressureDoor = true;
        door.GetComponent<BoxCollider>().enabled = true;
        crate.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        DoorAudioSource.PlayOneShot(doorClosefx);
    }

    void Start()
    {
        DoorAudioSource = GetComponent<AudioSource>();
    }
}
