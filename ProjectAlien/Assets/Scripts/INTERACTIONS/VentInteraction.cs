﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VentInteraction : MonoBehaviour
{
    [SerializeField] AudioClip ventSoundfx;
    public AudioSource VentSound;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            gameObject.GetComponent<BoxCollider>().enabled = false;
            VentSound.PlayOneShot(ventSoundfx);
        }
    }

    void Start()
    {
        VentSound = GetComponent<AudioSource>();
    }
}
