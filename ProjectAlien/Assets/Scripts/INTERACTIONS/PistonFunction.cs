﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class PistonFunction : MonoBehaviour
{
    public bool insideTrigger;
    public bool platformGrounded = true;
    public Animator pistonRise;
    public GameObject player;
    [SerializeField] TextMeshProUGUI text;


    private void Update()
    {
        if (insideTrigger)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                print("PRESSING THE MF BUTTON");
                PlayPistonAnimation();
            }
        }
        else
        {
            text.enabled = false;
        }   
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            text.enabled = true;
            insideTrigger = true;
        }      
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            insideTrigger = false;
        }
    }

    void PlayPistonAnimation()
    {
        if (platformGrounded)
        {
            text.enabled = false;
            pistonRise.SetTrigger("ActivateUp");
        }
    }

    void PistonBegin()
    {
        platformGrounded = false;
        player.GetComponent<PlayerControl>().enabled = false;        
    }


    void PistonEnd()
    {
        platformGrounded = true;
        player.GetComponent<PlayerControl>().enabled = true;
        pistonRise.SetTrigger("Reset");

    }
    
}
