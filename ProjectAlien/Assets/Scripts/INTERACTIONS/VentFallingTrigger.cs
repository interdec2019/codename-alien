﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VentFallingTrigger : MonoBehaviour
{

    public bool played;
    public bool bossKilled;
    public GameObject vent;

    public Animator animator;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            PlayAnimation();
        }
    }

    void PlayAnimation()
    {
        if (!played && bossKilled)
        {
            animator.SetTrigger("Activate2");
            vent.GetComponent<BoxCollider>().enabled = false;
            played = true;
        }

    }
}
