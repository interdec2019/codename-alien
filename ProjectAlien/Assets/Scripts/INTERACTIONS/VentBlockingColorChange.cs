﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VentBlockingColorChange : MonoBehaviour
{
    public Material solid;
    public Material transparent;
    public bool isTransparent;
    
    public GameObject ventBlocker;


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!isTransparent)
            {
                TurnTransparent();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (isTransparent)
            {
                ventBlocker.GetComponent<MeshRenderer>().material = solid;

                isTransparent = false;
                print("INSIDE");
            }
        }
            
    }

    void TurnTransparent()
    {
        ventBlocker.GetComponent<MeshRenderer>().material = transparent;
        isTransparent = true;
    }

}
