﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyPickup : MonoBehaviour
{
    bool insideTrigger;
    bool pickedUpKey;

    public GameObject door;


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            PickUpKey();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        insideTrigger = false;
    }

    void PickUpKey()
    {
        door.GetComponent<DoorOpeningClosing>().isLocked = false;
        door.GetComponent<DoorOpeningClosing>().isUnLocked = true;
        Destroy(gameObject);
    }

}
