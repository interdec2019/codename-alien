﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpeningClosing : MonoBehaviour
{
    [SerializeField] AudioClip doorOpenfx;
    [SerializeField] AudioClip doorClosefx;
    [SerializeField] AudioClip accessDeniedfx;
    [SerializeField] AudioClip accessGrantedfx;

    [SerializeField] GameObject newCenter;
    public AudioSource DoorOpenClose;
    public Animator doorOpen;
    public Animator doorClose;

    public Material ogMat;
    public Material green;
    public Material red;

    public GameObject lightLeft;
    public GameObject lightRight;

    public bool isLocked;
    public bool isUnLocked;
    public bool unlockedDoor;

    public bool isPressureDoor;

    //GameObject player;
    //float distance;
    //[SerializeField] float distanceT;

    //bool wentThroughDoor;

    //private void Awake()
    //{
    //    player = GameObject.Find("Player");
    //}
    // void FindPlayerDistance()
    //{
    //    distance = Vector3.Distance(player.transform.position, newCenter.transform.position);
    //}
    //private void Update()
    //{
    //    FindPlayerDistance();
    //    Debug.Log(distance);
    //    if (distance <=distanceT)
    //    {            
    //            if (isUnLocked)
    //            {
    //                OpenDoor();
    //                DoorOpenClose.PlayOneShot(doorOpenfx);
    //            }

    //            if (isLocked)
    //            {
    //                LockedDoorEnter();
    //                DoorOpenClose.PlayOneShot(accessDeniedfx);
    //            }

    //            if (isPressureDoor)
    //            {
    //                LockedDoorEnter();
    //                DoorOpenClose.PlayOneShot(accessDeniedfx);
    //            }
    //        wentThroughDoor = true;
    //    }
    //    else
    //    {if (wentThroughDoor)
    //        {
    //            if (isUnLocked)
    //            {
    //                CloseDoor();
    //                DoorOpenClose.PlayOneShot(doorClosefx);
    //            }

    //            if (isLocked)
    //            {
    //                LockedDoorExit();
    //            }

    //            if (isPressureDoor)
    //            {
    //                LockedDoorExit();
    //            }
    //        }
            
    //    }
        
    //}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (isUnLocked)
            {
                OpenDoor();
                DoorOpenClose.PlayOneShot(doorOpenfx);
            }

            if (isLocked)
            {
                LockedDoorEnter();
                DoorOpenClose.PlayOneShot(accessDeniedfx);
            }

            if (isPressureDoor)
            {
                LockedDoorEnter();
                DoorOpenClose.PlayOneShot(accessDeniedfx);
            }
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (isUnLocked)
            {
                CloseDoor();
                DoorOpenClose.PlayOneShot(doorClosefx);
            }

            if (isLocked)
            {
                LockedDoorExit();
            }

            if (isPressureDoor)
            {
                LockedDoorExit();
            }

        }
    }

    public void OpenDoor()
    {

         lightLeft.GetComponent<MeshRenderer>().material = green;
         lightRight.GetComponent<MeshRenderer>().material = green;
         doorOpen.SetTrigger("Activate1");
       // print("BOOBOO BITCHES");
    }

    public void CloseDoor()
    {
        doorClose.SetTrigger("Activate2");
        lightLeft.GetComponent<MeshRenderer>().material = ogMat;
        lightRight.GetComponent<MeshRenderer>().material = ogMat;
    }

    void LockedDoorEnter()
    {
        lightLeft.GetComponent<MeshRenderer>().material = red;
        lightRight.GetComponent<MeshRenderer>().material = red;
        //print("gg");
    }

    void LockedDoorExit()
    {
        lightLeft.GetComponent<MeshRenderer>().material = ogMat;
        lightRight.GetComponent<MeshRenderer>().material = ogMat;
    }

    void Start()
    {
        DoorOpenClose = GetComponent<AudioSource>();
    }
}


