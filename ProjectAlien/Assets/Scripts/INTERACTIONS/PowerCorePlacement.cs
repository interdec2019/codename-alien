﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerCorePlacement : MonoBehaviour
{
    public bool pickedupCore;
    bool placedCore;
    bool insideTrigger;

    public Material green;
    public GameObject powerCore;
    public GameObject cable;
    public GameObject door;


    private void Update()
    {
        if (insideTrigger)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                PlacePowerCore();
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            insideTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        insideTrigger = false;
    }

    void PlacePowerCore()
    {
        if (pickedupCore && !placedCore)
        {
            powerCore.GetComponent<MeshRenderer>().enabled = true;
            cable.GetComponent<MeshRenderer>().material = green;
            door.GetComponent<DoorOpeningClosing>().isUnLocked = true;
            door.GetComponent<DoorOpeningClosing>().isLocked = false;
        }

    }
}
