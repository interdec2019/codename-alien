﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainHubBehavior : MonoBehaviour
{
    bool playerIn;
    bool hubActive;

    [SerializeField] CanvasGroup interactionCanvas;

    [SerializeField] Animator bringScreenUp;

    void Start()
    {
        GameObject.Find("MainHubCanvas");
        HideInteractionButton();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            playerIn = true;
            DisplayInteractionButton();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            playerIn = false;
            HideInteractionButton();
            TurnScreenOff();
        }
    }

    void FixedUpdate()
    {
        if (Input.GetButtonDown("Action"))
        {
            if (playerIn)
            {
                if (!hubActive)
                {
                    TurnScreenOn();
                    
                }
                else if (hubActive)
                {
                    TurnScreenOff();
                }
                
            }
        }
    }

    void TurnScreenOn()
    {
        bringScreenUp.SetBool("Activate", true);
        hubActive = true;
    }

    void TurnScreenOff()
    {
        bringScreenUp.SetBool("Activate", false);
        hubActive = false;
    }

    void DisplayInteractionButton()
    {
        interactionCanvas.alpha = 1f;
    }

    void HideInteractionButton()
    {
        interactionCanvas.alpha = 0f;
    }
}
