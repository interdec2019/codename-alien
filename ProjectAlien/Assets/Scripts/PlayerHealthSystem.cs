﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class PlayerHealthSystem : MonoBehaviour, IDamageable
{
    #region Variables
    [SerializeField] float originalHealth = 20f;
    public float currentHealth { get; set; }
    [SerializeField] bool killable;
    public Vector3 lastCheckpoint
    {
        get;
        private set;
    }
    Vector3 mainHub;
    bool inRegenStation;
    [SerializeField] float timeToRegen=1.5f;
    #endregion

    #region UI/VFX
    [SerializeField] Image healthBarImage;
    [SerializeField] TextMeshProUGUI healthText;
    [SerializeField] Image ammoImage;

    [SerializeField] Material originalMaterial;
    [SerializeField] Material teleportMat;
    Renderer[] armourPieces;
    Renderer[] weaponPieces;
    GameObject model;
    GameObject weapon;
    #endregion

    #region ExteriorScripts
    [SerializeField] MenuHandler menuScript;
    //[SerializeField] LoadLevelManager levelManagerScript;
    PlayerControl playerControlScript;
    #endregion

    
    private void OnEnable()
    {
        GameManager.OnPlayerDataLoaded += LoadingLastCheckPoint;
    }

    private void OnDisable()
    {
        GameManager.OnPlayerDataLoaded -= LoadingLastCheckPoint;
    }

    void Start()
    {
        //GameManager.instance.LoadPlayerIntoLevel();
        currentHealth = originalHealth;
        playerControlScript = GetComponent<PlayerControl>();
        GetPlayerRenderer();
        //reloading = false;
    }
    private void Update()
    {
        if (killable)
        {
            if (currentHealth <= 0)
            {
                menuScript.ActivateDeathMenu();
            }
        }
        if (inRegenStation)
        {
            HealPlayer();
        }
    }

    public void TakeDamage(float damage)
    {
        
        if (!playerControlScript.usingShield)
        {
            currentHealth -= damage;
            UpdateHealthUI();
        }
        
    }
    void UpdateHealthUI()
    {
        healthBarImage.fillAmount = currentHealth / originalHealth;
        healthText.text = ((currentHealth / originalHealth) * 100f).ToString("0") + "%";

        if ((currentHealth / originalHealth) <= 0.3f)
        {
            healthBarImage.color = Color.red;
            healthText.color = Color.red;
        }
        else
        {
            healthBarImage.color = Color.green;
            healthText.color = Color.cyan;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Checkpoint")
        {
            InteractWithCheckpoint(other.gameObject.GetComponent<Checkpoint>().spawnPoint.transform.position);
            GameManager.instance.UpdatePlayerValues(currentHealth, lastCheckpoint);           
            
        }

        if (other.gameObject.tag == "Teleporter")
        {
            InteractWithCheckpoint(other.gameObject.GetComponent<TeleporterToMainHub>().hubPos.transform.position);
            GameManager.instance.UpdatePlayerValues(currentHealth, lastCheckpoint);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Checkpoint")
        {
            inRegenStation = false;
        }
    }



    void InteractWithCheckpoint(Vector3 _pos)
    {        
        lastCheckpoint = _pos;
        inRegenStation = true;
        UpdateHealthUI();
    }

    public void LoadingLastCheckPoint(PlayerData data)
    {
        StartCoroutine(LoadPlayerIntoLastCheckPoint(data));
    }

    public void HealPlayer()
    {
        if (currentHealth != originalHealth)
        {
            currentHealth = Mathf.Lerp(currentHealth, originalHealth, Time.deltaTime * timeToRegen); //nailed it

            //currentHealth = currentHealth + Time.deltaTime*timeToRegen;
        } else if (currentHealth >= originalHealth)
        {
            currentHealth = originalHealth;
        }
        UpdateHealthUI();

        
        
    }
    

    public void GetPlayerRenderer()
    {
        model = GameObject.Find("Leyssa");
        weapon = GameObject.Find("Weapon");
        armourPieces = model.GetComponentsInChildren<Renderer>();
        weaponPieces = weapon.GetComponentsInChildren<Renderer>();
    }
    public IEnumerator LoadPlayerIntoLastCheckPoint(PlayerData data)
    {
        yield return new WaitForSeconds(1f);
        //sound cue
        gameObject.GetComponent<PlayerControl>().enabled = false;
        StartCoroutine(TeleportOut());
        yield return new WaitForSeconds(1.5f);
        //currentHealth = data.playerHealth;////////// to be rewired to recoverhealth
        Vector3 temp = new Vector3(data.currentPosition[0], data.currentPosition[1], data.currentPosition[2]);
        transform.position = temp;
        gameObject.GetComponent<PlayerControl>().enabled = true;
        //StartCoroutine(TeleportIn());

        //yield return new WaitForSeconds(1.5f);//////temop fix
        //ResetMaterials();
        yield return null;
    }
   

    public IEnumerator TeleportOut()
    {
        

        foreach (Renderer piece in armourPieces)
        {
            //piece.material = teleportMat;
            if (!piece.gameObject.GetComponent<Teleporting>())
            {
                piece.gameObject.AddComponent<Teleporting>();
                piece.GetComponent<Teleporting>().ActivateTP();
            }
            else if (piece.gameObject.GetComponent<Teleporting>())
            {
                piece.GetComponent<Teleporting>().ActivateTP();
            }

        }
        foreach (Renderer piece in weaponPieces)
        {
            //piece.material = teleportMat;
            if (!piece.gameObject.GetComponent<Teleporting>())
            {
                piece.gameObject.AddComponent<Teleporting>();
                piece.GetComponent<Teleporting>().ActivateTP();
            }
            else if (piece.gameObject.GetComponent<Teleporting>())
            {
                piece.GetComponent<Teleporting>().ActivateTP();
            }

        }


        yield return null;
        
    }
    public IEnumerator TeleportIn()
    {
        //armourPieces = model.GetComponentsInChildren<Renderer>();

        foreach (Renderer piece in armourPieces)
        {

            //piece.material = teleportMat;
            if (!piece.gameObject.GetComponent<Teleporting>())
            {
                piece.gameObject.AddComponent<Teleporting>();
                piece.GetComponent<Teleporting>().DectivateTP();
            }
            else if (piece.gameObject.GetComponent<Teleporting>())
            {
                piece.GetComponent<Teleporting>().DectivateTP();
            }
        }
        foreach (Renderer piece in weaponPieces)
        {

            //piece.material = teleportMat;
            if (!piece.gameObject.GetComponent<Teleporting>())
            {
                piece.gameObject.AddComponent<Teleporting>();
                piece.GetComponent<Teleporting>().DectivateTP();
            }
            else if (piece.gameObject.GetComponent<Teleporting>())
            {
                piece.GetComponent<Teleporting>().DectivateTP();
            }
        }
        //yield return new WaitForSeconds(0.1f);

        yield return null;
    }

    public void ResetMaterials()
    {
        armourPieces = model.GetComponentsInChildren<Renderer>();

        foreach (Renderer piece in armourPieces)
        {
            piece.GetComponent<Teleporting>().ResetMaterial();
            
        }
    }
}

