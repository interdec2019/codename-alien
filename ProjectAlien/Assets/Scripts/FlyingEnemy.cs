﻿using UnityEngine;

public class FlyingEnemy : MonoBehaviour //,IDamageable Gabriel
{
    #region Serialized Fields
    [SerializeField] Transform firePoint = null;
    [SerializeField] Rigidbody bulletPrefeb = null;
    #endregion


    #region privatevars
    private GameObject player = null;
    private Vector3 playerPos;
    RaycastHit hit;
    Quaternion lastRot;
    #endregion

    #region publicvars
    public LayerMask mask;
    public float movespeed;
    public float chaseSpeed;
    public float fireSpeed;
    public float fireRate;
    public float nextFire;
    public float rayCastLenght;
    public Vector3 offset;
    public float health = 25f;
    public float shootDistance = 10f;
    public float chasingDistance = 20f;
    #endregion

    private void Awake()
    {
        player = FindObjectOfType<PlayerControl>().gameObject;
        //- Gabriel: This makes enemy be always in player's Z axis
        playerPos = player.transform.position;
        transform.position = new Vector3(transform.position.x, transform.position.y, playerPos.z);
        //-
        lastRot = transform.rotation;
    }

    // Start is called before the first frame update
    void Start()
    {
        nextFire = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsLineOfSight() && !isPLayerInChaseRange() && !IsPlayerInShootingRange())
        {

            Patrol();

        }
        else if (IsLineOfSight() && !isPLayerInChaseRange() && !IsPlayerInShootingRange())
        {
            Patrol();

        }
        else if (!IsLineOfSight() && isPLayerInChaseRange() && !IsPlayerInShootingRange())
        {
            Patrol();
        }
        else if (!IsLineOfSight() && !isPLayerInChaseRange() && IsPlayerInShootingRange())
        {
            Patrol();
        }
        else if (!IsLineOfSight() && isPLayerInChaseRange() && IsPlayerInShootingRange())
        {
            Patrol();
        }
        else if (isPLayerInChaseRange() && IsLineOfSight())
        {
            MoveTowardsPlayer();
            if (IsLineOfSight() && IsPlayerInShootingRange())
            {
                AimAtPlayer();
                Shoot();                
            }
        }   
    }

    void Patrol()
    {
        transform.Translate(Vector3.forward * movespeed * Time.deltaTime, Space.Self);
        if (Physics.Raycast(transform.transform.position, transform.forward, out hit, rayCastLenght))
        {
            transform.Rotate(0, 180, 0);
        }
        lastRot = transform.rotation;

        Debug.DrawRay(transform.position, transform.forward * rayCastLenght, Color.black);
        //print("Patrolling");
    }


    bool isPLayerInChaseRange()
    {

        float distance = Vector3.Distance(player.transform.position + offset, transform.position);

        if (distance <= chasingDistance)
        {
            return true;
        }
        return false;
    }


    void MoveTowardsPlayer()
    {
        transform.rotation = lastRot;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z), 0.8f * Time.deltaTime);
    }


    bool IsPlayerInShootingRange()
    {
        float distance = Vector3.Distance(player.transform.position + offset, transform.position);
        if (distance <= shootDistance)
        {
            return true;
        }
        return false;
    }

    void AimAtPlayer()
    {
        transform.LookAt(player.transform.position + offset);
    }


    void Shoot()
    {
        if (Time.time > nextFire)
        {
            Rigidbody rb = Instantiate(bulletPrefeb, firePoint.position, firePoint.rotation);
            rb.velocity = transform.forward * fireSpeed;
            Destroy(rb.gameObject, 1f);
            nextFire = Time.time + fireRate;
        }

    }


    bool IsLineOfSight()
    {
        Debug.DrawLine(transform.position, player.transform.position + offset,Color.blue);
        if (Physics.Linecast(transform.position, player.transform.position + offset, mask, QueryTriggerInteraction.Ignore))
        {
            transform.rotation = lastRot;
            return false;
        }
        else
        {
            return true;
        }
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, shootDistance);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, chasingDistance);
    }

    //public void TakeDamage(float damage) // Gabriel
    //{
    //    health -= damage;
    //}
    //void HandleDeath()
    //{
    //    if (health <= 0f)
    //    {
    //        Destroy(gameObject);
    //    }
    //}
}
