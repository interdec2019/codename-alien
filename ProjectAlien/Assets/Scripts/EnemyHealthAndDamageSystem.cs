﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthAndDamageSystem : MonoBehaviour, IDamageable
{
    #region Variables
    [SerializeField] float originalHealth = 6f;
    float currentHealth;
    [SerializeField] Renderer skin;
    float dissolve = 0;
    bool dying;

    [SerializeField] bool typeFlyer;
    #endregion

    #region Player Related Variables
    [SerializeField] float damageToPlayerWhilePushing = 0.02f;
    [SerializeField] float damageToPlayerUponImpact=1;

    [SerializeField] Animator animator;
    Component damageable; //player
    //GameObject player; // useless for now
    //Vector3 PlayerPos; // same
    #endregion

    #region UI 
    [SerializeField] Image healthBar;
    [SerializeField] CanvasGroup healthBarHolder;
    #endregion

    
    void Start()
    {
        //player = GameObject.Find("Player");
        currentHealth = originalHealth;
    }
    void Update()
    {
        DisssolveEnemy();
        FadeOutHeathBar();
    }
    
    private void OnCollisionEnter(Collision collision)//deal damage to player
    {
        //Bruno:- For collison of destructable objects
        if (collision.gameObject.tag == "Player")
        {
            damageable = collision.collider.GetComponent(typeof(IDamageable));
            DealDamage(damageToPlayerUponImpact);
        }
        
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            DealDamage(damageToPlayerWhilePushing);
        }
    }

    public void TakeDamage(float damage)
    {
        SetHealthFillAmount();
        currentHealth -= damage/5f;
        SetHealthFillAmount();
        HandleDeath();
    }
    void DealDamage(float _damage)
    {
        if (damageable)
        {
            (damageable as IDamageable).TakeDamage(_damage);
        }
    }

    void HandleDeath()
    {
        if (currentHealth <= 0f)
        {
            currentHealth = 0f;
            StartCoroutine(TriggerDeathAnim());            
        }
    }
    
    void DisssolveEnemy()
    {if (dying)
        {
            dissolve += Time.deltaTime / 1.5f;
            skin.material.SetFloat("_Burn0", dissolve);
        }
    }

    IEnumerator TriggerDeathAnim()
    {
        float _timeToDie = 1.5f;
        if (!typeFlyer)
        {
            GetComponent<CrawlerEnemy>().enabled = false;
            animator.SetBool("Dead", true);
        }
        else
        {
            GetComponent<FlyingEnemy>().enabled = false;
        }
        dying = true;
        yield return new WaitForSeconds(_timeToDie);
        
        gameObject.SetActive(false);
        yield return null;
    }
    void SetHealthFillAmount()
    {
        healthBarHolder.alpha=1;
        healthBar.enabled = true;
        healthBar.fillAmount = currentHealth / originalHealth;
    }

    void FadeOutHeathBar()
    {
        healthBarHolder.alpha -= 0.01f;
    }
    public void Resurect()
    {
        
        GetComponent<CrawlerEnemy>().enabled = true;
        dying = false;
        skin.material.SetFloat("_Burn0", 0f);
        currentHealth = originalHealth;
        SetHealthFillAmount();
    }


}
