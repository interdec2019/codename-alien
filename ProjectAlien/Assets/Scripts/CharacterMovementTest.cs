﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovementTest : MonoBehaviour
{
    [SerializeField] float runSpeed = 30f;
    Rigidbody rigid;
    Vector3 velocity = Vector3.zero;
    float horizontalMove;
    bool jumpInput;
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;	// How much to smooth out the movement

    bool grounded=true;

    //[SerializeField]
    private float m_JumpForce = 320f;

    void Start()
    {
        rigid = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        horizontalMove = Input.GetAxis("Horizontal")*runSpeed ;

        // Move the character by finding the target velocity
        Vector2 targetVelocity = new Vector2(horizontalMove , rigid.velocity.y);
        // And then smoothing it out and applying it to the character
        rigid.velocity = Vector3.SmoothDamp(rigid.velocity, targetVelocity, ref velocity, m_MovementSmoothing);
        Debug.Log(targetVelocity);

        jumpInput = Input.GetButtonDown("Jump");
        if (jumpInput && grounded)
        {
            //grounded = false;
            //rigid.angularDrag = 0.0f;
            rigid.AddForce(new Vector2(0f, m_JumpForce));
            rigid.drag = 0.0f;
        }
       
    }
}
