﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProjectileSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject projectile;
    Vector3 spawnerPos;

    [SerializeField] float maxAmmo=15f;
    float currentAmmo;

   
    [SerializeField] float fireRate=0.2f;
    bool shooting;

    bool reloading;
    [SerializeField] float reloadTime;
    [SerializeField] Image ammoImage;
    [SerializeField] Image reloadImage;

    [SerializeField] GameObject muzzleFlash;
    Color _originalColour;

    public AudioSource gunfx;
    [SerializeField] AudioClip shootingfx;
    [SerializeField] AudioClip reloadingfx;
    [SerializeField] AudioClip gunReadyfx;
    

    private void Start()
    {
        currentAmmo = maxAmmo;
        muzzleFlash.SetActive(false);
        _originalColour = ammoImage.color;
        gunfx = GetComponent<AudioSource>();

    }

    void Update()
    {
        HandleShooting();
        ReloadOnCommand();
        ReloadAmmoImageFill();
    }

    private void ReloadOnCommand()
    {
        if (Input.GetButton("Reload") && currentAmmo!=maxAmmo)
        {
            
            reloading = true;
            StartCoroutine(ReloadAmmo());
        }
    }

    private void OnEnable()
    {
        shooting = false;
        if (reloading)
        {
            StartCoroutine(ReloadAmmo()); 
        }
    }

    private void OnDisable()
    {
        shooting = false;
    }

    void HandleShooting()
    {
        if (!reloading)
        {
            if (!shooting)
            {
                if (Input.GetAxis("Fire1") >= 0.1f)
                {
                    spawnerPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
                    Instantiate(projectile, spawnerPos, transform.rotation); //spawns projectile in spawner pos position and adds the spawner rotation to it
                    gunfx.PlayOneShot(shootingfx);

                    muzzleFlash.SetActive(true);
                    shooting = true;
                    currentAmmo--;
                    ammoImage.fillAmount = currentAmmo / maxAmmo;
                    StartCoroutine(HandleFireRate());
                }
            }
        }
        else if (Input.GetAxis("Fire1") >= 0.1f && reloading)
        {
            //gunfx.PlayOneShot(shootingWhileReloadingfx);
        }

        if (currentAmmo <= 0f && !reloading)
        {

            reloading = true;
            StartCoroutine(ReloadAmmo());
        }
    }


    IEnumerator HandleFireRate()
    {
        yield return new WaitForSeconds(fireRate);
        shooting = false;
        muzzleFlash.SetActive(false);
        yield return null;
    }

    IEnumerator ReloadAmmo()
    {
        gunfx.PlayOneShot(reloadingfx);
        reloadImage.fillAmount = 0f;
        reloadImage.color = Color.red;
        ammoImage.color = Color.red;
        currentAmmo = maxAmmo;
        ammoImage.fillAmount = currentAmmo / maxAmmo;
        yield return new WaitForSeconds(reloadTime);               
        
        reloading = false;
        ammoImage.color = _originalColour;
        reloadImage.color = _originalColour;
        gunfx.PlayOneShot(gunReadyfx);
        yield return null;
    }

    void ReloadAmmoImageFill()
    {
        if (reloading)
        {
            reloadImage.fillAmount += Time.deltaTime / reloadTime;
        }

    }
}
