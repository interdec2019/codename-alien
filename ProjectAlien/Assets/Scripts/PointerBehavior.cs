﻿using UnityEngine;


public class PointerBehavior : MonoBehaviour
{
    
    [SerializeField] bool joystick;
    [SerializeField] GameObject player;
    Vector3 pointerPos;    
    Vector3 finalPointerPos;
    // Update is called once per frame
    void Update()
    {
        finalPointerPos = transform.position;
        Debug.Log(finalPointerPos);
        if (!joystick)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                pointerPos = hit.point;
                pointerPos.z = transform.position.z;
                transform.position = pointerPos;
            }
        }
        

        if (joystick)
        {
            float _horizontal = Input.GetAxis("JoystickX");
            float _vertical = Input.GetAxis("JoystickY");

            pointerPos = new Vector3(_horizontal, _vertical, 0f);//WIP
            
            transform.position += pointerPos;
           

        }

       
    }
}
