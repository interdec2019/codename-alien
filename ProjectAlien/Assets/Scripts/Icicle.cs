﻿using UnityEngine;

public class Icicle : MonoBehaviour
{
    [SerializeField] float damageDealt=20f;
    [SerializeField] float lifeSpan= 0.2f;
    bool activated;

    Rigidbody rigid;

    private void Start()
    {
        rigid = gameObject.GetComponent<Rigidbody>();
        rigid.useGravity = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            rigid.useGravity = true;
            activated = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Component damageable = collision.collider.GetComponent(typeof(IDamageable));
        if (damageable)
        {
            (damageable as IDamageable).TakeDamage(5f);
            DestroyIcicle(0f);
        }
        if (activated)
        {
            DestroyIcicle(1f);
        }
    }
    void DestroyIcicle(float lifeSpan)
    {
        Destroy(gameObject,lifeSpan );
    }
}
