﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public float playerCurrentHealth { get; private set; }
    public Vector3 playerCurrentPosition { get; private set; }
    public int currentLevelIndex { get; private set; }
    public PlayerData playerData { get; private set; }



    #region Delegates & Events
    public delegate void PlayerDataLoaded(PlayerData data);
    public static event PlayerDataLoaded OnPlayerDataLoaded;
    public delegate void ReturnToMainHub();
    #endregion

    private void Awake()
    {
        #region Singleton
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
        #endregion
        DontDestroyOnLoad(gameObject);
    }

    public void UpdatePlayerValues(float currenthealth, Vector3 currentPosition)
    {
        playerCurrentPosition = currentPosition;
        playerCurrentHealth = currenthealth;
        SavePlayerData();
    }


    public void LoadScene(int sceneIndex)
    {
        currentLevelIndex = sceneIndex;
        SceneManager.LoadScene(currentLevelIndex);
    }

    public void SavePlayerData()
    {
        SaveSystem.SavePlayer(this);
    }

    public void LoadPlayerIntoLevel() // PRevious was Load 
    {
        //float _scene =SceneManager.GetActiveScene().buildIndex;
        //if (_scene != 1f)
        //{
        //    SceneManager.LoadScene(1); // mian level must be build index 1
        //}
        playerData = SaveSystem.LodaData();
        OnPlayerDataLoaded(playerData);
    }
   
}
