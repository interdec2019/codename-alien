﻿using UnityEngine;
using System.Collections;

public class DestructableObjects : MonoBehaviour, IDamageable
{
    public float Health;
    public GameObject destroyed_ob;

    public float explosionForce, radius, upwardScale;

    void Update()
    {
        //if (Input.GetButtonDown("Interact"))
        //{
        //    this.transform.SendMessageUpwards("GetBulletDamage", 20, SendMessageOptions.DontRequireReceiver);
        //}
    }

    public void TakeDamage(float damage)
    {
        Health -= damage;

        if (Health <= 0)
        {
            GameObject destroy_ob = Instantiate(destroyed_ob, this.transform.position, this.transform.rotation) as GameObject;
            Collider[] pieces = Physics.OverlapSphere(transform.position, 2f);
            foreach (var piece in pieces)
            {
                if (piece.GetComponent<Rigidbody>())
                {
                    piece.GetComponent<Rigidbody>().AddExplosionForce(explosionForce, transform.position, radius, upwardScale);
                }
            }
            GetComponent<Collider>().enabled = false;
            GetComponent<Renderer>().enabled = false;
            Destroy(destroy_ob, 3f);
        }
    }
}
