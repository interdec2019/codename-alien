﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TeleporterToMainHub : MonoBehaviour
{
    public GameObject hubPos;
    [SerializeField] TextMeshProUGUI text;

    bool playerIn;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            playerIn = true;
            text.enabled = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            playerIn = false;
            text.enabled = false;
        }
    }

    void Update()
    {
        if (playerIn)
        {if (Input.GetButton("Interact"))
            {
                GameManager.instance.LoadPlayerIntoLevel();
                text.enabled = false;
                playerIn = false;
            }

        }
    }
}
