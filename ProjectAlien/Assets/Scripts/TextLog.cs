﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TextLog : MonoBehaviour
{
    TextMeshProUGUI textLog;
    //[SerializeField] AudioClip audioToPlay;
    [SerializeField] string textToDisplay;
    [SerializeField] float timeToDisplay= 5f;

    [SerializeField] bool activateOnce;

    [SerializeField] Animator textLogFade;
    bool activated;

    private void OnTriggerEnter(Collider other)
    {
        if (activateOnce)
        {
            if (!activated)
            {
                if (other.tag == "Player")
                {
                    textLog = GameObject.Find("TextLog").GetComponent<TextMeshProUGUI>();
                    StartCoroutine(DisplayTextLog());
                    activated = true;
                }
            }
            
        }
        else
        {
            if (other.tag == "Player")
            {
                textLog = GameObject.Find("TextLog").GetComponent<TextMeshProUGUI>();
                StartCoroutine(DisplayTextLog());
            }
        }        
    }    

    IEnumerator DisplayTextLog()
    {
        //StopAllCoroutines();
        textLog.text = textToDisplay;
        textLogFade.SetBool("Display", true);
        yield return new WaitForSeconds(timeToDisplay);
        textLogFade.SetBool("Display", false);
        
        
        yield return null;
    }
   
}
