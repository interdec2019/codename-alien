﻿using UnityEngine;

public class AcidPool : MonoBehaviour
{
    PlayerHealthSystem playerScript;
    [SerializeField] bool playerIsIn;
    float damageDealt=5f;

    void Update()
    {
        
        DealDamage();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            playerScript = other.GetComponent<PlayerHealthSystem>();
            playerIsIn = true;
            
        }
        
        
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            playerIsIn = false;

        }

    }
    void DealDamage()
    {   if (playerIsIn)
        {
            playerScript.TakeDamage(damageDealt * Time.deltaTime);
        }
        
    }
}
