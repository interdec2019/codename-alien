﻿using System.Collections;
using UnityEngine;

public class CrawlerEnemy : MonoBehaviour
{
    
    

    #region CrawlerStats //Serialzed Fields
    Rigidbody crawlerRigid;
    
    [SerializeField] float originalEnemySpeed = 3f;
    [SerializeField]float enemySpeedNearPlayer = 7f;
    [SerializeField] float minDistanceToPlayerToGoCrazy = 9f;
   
    #endregion
    #region Inner Variables And Input
    Vector3 rayDirection;
    bool _horizontal = true;
    float enemySpeed;
    float distanceToPlayer;
    float enemyDirection;
    #endregion
    #region Player Related Variables
    GameObject player;
    Vector3 playerPos;
    bool playerDetected;
    bool playerBeingAttacked;
    #endregion
    public LayerMask mask;   

    
    void Start()
    {
        crawlerRigid = gameObject.GetComponent<Rigidbody>();
        player = GameObject.Find("Player");
        playerPos = player.transform.position;
        transform.position = new Vector3(transform.position.x,transform.position.y, playerPos.z);
        rayDirection = Vector3.right *1;
        enemyDirection = 1f;
        enemySpeed = originalEnemySpeed;
    }

   
    void Update()
    {
        DetectDisctanceToPlayer();
        MoveSideways();
        RayDetection();        
    }
    bool IsPlayerToTheLeft() // this tells us if the player is to the left or the right of the crawler
    {
        float _PlayerXpos;
        float _enemyXpos;
        _enemyXpos= transform.position.x;
        _PlayerXpos = playerPos.x;

        if (_PlayerXpos > _enemyXpos)
        {
            return(false);
        }
        else
        {
            return(true);
        }
    }

    private void DetectDisctanceToPlayer()
    {
        playerPos = player.transform.position;
        distanceToPlayer = Vector3.Distance(playerPos, transform.position);

        if (distanceToPlayer < minDistanceToPlayerToGoCrazy)
        {
            playerDetected = true;            
        }
        else
        {
            playerDetected = false;            
        }
        ActionWhenDetectingPlayer();
    }

    private void ActionWhenDetectingPlayer()
    {
        if (playerDetected)
        {
            enemySpeed = enemySpeedNearPlayer;
            AttackPlayer();//attack player will only be activated once per encounter

        }
        else
        {
            enemySpeed = originalEnemySpeed;
        }
    }

    private void AttackPlayer()
    {
        if (!playerBeingAttacked)
        {
            if (IsPlayerToTheLeft())
            {
                enemyDirection = -1;
                rayDirection = Vector3.left;
                playerBeingAttacked = true;
            }
            else if (!IsPlayerToTheLeft())
            {
                enemyDirection = 1;
                rayDirection = Vector3.right;
                playerBeingAttacked = true;
            }
        }
    }

    void RandomActionChoice()
    {
        float _choice = Random.Range(1f, 5f);
        if (_choice > 3f)
        {
            StartCoroutine(ChangeDirection());
            
        }
        if (_choice < 3f)
        {
            //StartCoroutine(MoveUpwards());
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            RandomActionWhenCollidingWithPlayer();
        }
    }

    void RandomActionWhenCollidingWithPlayer()
    {
        
        float _choice = Random.Range(0f, 10f);
        if (_choice < 7f)
        {
            //StartCoroutine(ChangeDirection());
        }
    }

    private void RayDetection()
    {
        RaycastHit hit;

        Debug.DrawRay(GetComponent<Collider>().bounds.center, rayDirection * 0.55f, Color.black);

        if (Physics.Raycast(GetComponent<Collider>().bounds.center, rayDirection, out hit, 0.5f, mask))
        {
            if (hit.collider) //.tag != "Player" || hit.collider.tag != "Enemy"
            {
                RandomActionChoice();
            }
        }
        else
        {
            float _choice = Random.Range(0f, 1000f);
            if (_choice < 2f)
            {
                StartCoroutine(ChangeDirection());
            }
        }   
    }

    void MoveSideways()
    {
        if (_horizontal)
        {
            crawlerRigid.velocity = new Vector3 (enemyDirection * enemySpeed,crawlerRigid.velocity.y,0f);
        }

    }
  
    IEnumerator ChangeDirection()
    {
        yield return new WaitForSeconds(0.2f);
        rayDirection *= -1;
        enemyDirection *= -1;
        yield return null;
    }

    IEnumerator MoveUpwards()
    {
        
        _horizontal = false;
        crawlerRigid.useGravity = false;
        crawlerRigid.velocity = Vector3.up * 2f;
        yield return new WaitForSeconds(0.6f);
        crawlerRigid.useGravity = true;
        _horizontal = true;
        yield return null;
    }
    
}
