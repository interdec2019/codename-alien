﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public GameObject spawnPoint;
    public AudioSource regenrationStationfx;
    [SerializeField] AudioClip regeneratehealthfx;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            regenrationStationfx.PlayOneShot(regeneratehealthfx);
        }
    }

    private void Start()
    {
        regenrationStationfx = GetComponent<AudioSource>();
    }
}
