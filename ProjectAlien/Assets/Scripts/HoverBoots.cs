﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;


public class HoverBoots : MonoBehaviour
{
    float _doubleTapTimeD;
    Rigidbody rb;
    bool doubleTap = false;
    PlayerControl pc;
    bool usingBoots = false;
    bool once = false;

    #region
    [SerializeField] Image imgHoverEnergyBar = null;
    #endregion

    #region Private vars
    private CanvasGroup hoverBarCanvasGroup = null;
    #endregion

    #region PublicField
    public float maxHealth = 100f;
    public float currentHealth = 0f;
    public float regenrationRatePerSecond = 0.01f;
    public float drainingRatePerSeconds = 0.01f;
    public float startTime = 1f;
    public float nextCall = 0.1f;
    public bool isLocked {get; private set;}
    #endregion

    void Start()
    {
        currentHealth = maxHealth;
        isLocked = false;
        rb = GetComponent<Rigidbody>();
        pc = GetComponent<PlayerControl>();
        hoverBarCanvasGroup = imgHoverEnergyBar.GetComponentInParent<CanvasGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        #region doubleTap
        if (Input.GetButtonDown("Jump"))
        {
            //print("Doubletap:" + doubleTap);
            //print("ïsLocked:" + isLocked);
            if (Time.time < _doubleTapTimeD + .3f)
            {
                if (!doubleTap && !isLocked)
                {
                    doubleTap = true;
                    ActivateHoverBoots();
                    
                }
                else
                {
                    doubleTap = false;
                    DeactivateBoots();
                    
                }
            }
            _doubleTapTimeD = Time.time;
        }

        if (usingBoots)
        {
            DepletHoverEnergy();
        }
        else
        {
            RegenerateHoverEnergy();
        }
        #endregion
    }

    private void FixedUpdate()
    {
        if (doubleTap && !isLocked)
        {
            if (currentHealth > 0)
            {
                StopAllCoroutines();
                StartCoroutine(ShowBar());
            }
        }

        if (!doubleTap && isLocked)
        {
            if (currentHealth <= maxHealth)
            {
                StopAllCoroutines();
                StartCoroutine(ShowBar());
            }
        }
        if (pc.onTheGround)
        {
            DeactivateBoots();
        }
    }

    void ActivateHoverBoots()
    {
        usingBoots = true;
        pc.UsingHoverBoots();
        //rb.velocity = new Vector3(rb.velocity.x, 1f, rb.velocity.z);//harcoded
        rb.useGravity = false;
        rb.drag = 2f;
    }

    void DeactivateBoots()
    {
        usingBoots = false;
        pc.NotUsingHoverBoots();
        rb.useGravity = true;
        //isLocked = true;
    }

    void DepletHoverEnergy()
    {
        if (currentHealth > 0)
        {
            //currentHealth -= (currentHealth * drainingRate);
            currentHealth -= drainingRatePerSeconds * Time.deltaTime;
            imgHoverEnergyBar.fillAmount = currentHealth / maxHealth;
            if (currentHealth <= 0f)
            {
                doubleTap = false;
                DeactivateBoots();
            }
        }
    }


    void RegenerateHoverEnergy()
    {
        if (currentHealth >= maxHealth)
        {
            isLocked = false;
            if(!once)
                StartCoroutine(HideBar());  
        }
        if (currentHealth < maxHealth)
        {
            currentHealth += regenrationRatePerSecond * Time.deltaTime;
            imgHoverEnergyBar.fillAmount = currentHealth / maxHealth;
            isLocked = true;
        }
    }

    IEnumerator ShowBar()
    {
        once = false;
        float timer = 0f;
        while (timer < 0.5f)
        {
            hoverBarCanvasGroup.alpha = Mathf.Lerp(hoverBarCanvasGroup.alpha, 1f, (timer / 0.5f));
            timer += Time.deltaTime;
            yield return null;
        }
        //StartCoroutine(HideBar());
    }

    IEnumerator HideBar()
    {
        once = true;
        float timer = 0f;
        while (timer < 1.5f)
        {
            hoverBarCanvasGroup.alpha = Mathf.Lerp(hoverBarCanvasGroup.alpha, 0f, (timer / 1.5f));
            timer += Time.deltaTime;
            yield return null;
        }
    }
}
