﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public float playerHealth;
    public int currentLevel;
    public float[] currentPosition;
    


    public PlayerData(GameManager manager)
    {
        currentLevel = 1;

        if (manager != null)
        {
            currentPosition = new float[3];
            currentPosition[0] = manager.playerCurrentPosition.x;
            currentPosition[1] = manager.playerCurrentPosition.y;
            currentPosition[2] = manager.playerCurrentPosition.z;

            //playerHealth = manager.playerCurrentHealth;
        }

    }
}
