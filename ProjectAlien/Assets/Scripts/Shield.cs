﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
public class Shield : MonoBehaviour
{
    #region SerializedFields
    [SerializeField] AnimationCurve curveDamagePower;
    [SerializeField] Transform firePoint = null;
    [SerializeField] Rigidbody energyForce = null;
    [SerializeField] GameObject playerShield = null;
    [SerializeField] Image shieldEnergyBar = null;
    [SerializeField] AudioClip shieldUsingfx;
    //[SerializeField] AudioClip shieldExpellingEngeryfx;
    [SerializeField] AudioClip shieldReadyfx;
    [SerializeField] AudioClip shieldDepletedfx;
    #endregion
    public AudioSource Shieldfx;
    #region PublicField
    public float appliedForceForEnergy = 10f;
    public float maxHealth = 100f;
    public float currentHealth = 0f;
    public float regenrationRate = 20f;
    public float drainingRate = 20f;
    public float startCall = 0.1f;
    public float nextCall = 0.05f;
    public bool isLocked { get; private set; }
    public bool isSpawned { get; private set; }
    public bool shieldBeingDeployed; //Gab
    #endregion


    #region
    private CanvasGroup energyBarCanvasGroup = null;
    PlayerControl playerControlScript; //Gab
    GameObject weapon;
    #endregion

    bool isSoundPlayed;
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        isLocked = false;
        isSpawned = false;
        energyBarCanvasGroup = shieldEnergyBar.GetComponentInParent<CanvasGroup>();
        playerControlScript = gameObject.GetComponent<PlayerControl>(); //Gab
        weapon = GameObject.Find("Weapon");
        Shieldfx = GetComponent<AudioSource>();
        isSoundPlayed = false;
    }


    // Update is called once per frame
    private void Update()
    {
        if(Input.GetButton("Fire2") && !isLocked)
        { if (playerControlScript.onTheGround) //Gab
            {
                
                playerShield.SetActive(true);
                DeployingShieldEnergy();
                CancelInvoke("RegenerateShieldEnergy");
                shieldBeingDeployed = true; //Gab
                weapon.SetActive(false);
            }
        }


        if (Input.GetButtonUp("Fire2") && !IsInvoking("RegenerateShieldEnergy"))
        {
            CancelInvoke("RegenerateShieldEnergy");
            InvokeRepeating("RegenerateShieldEnergy", startCall, nextCall);
            playerShield.SetActive(false);
            shieldBeingDeployed = false; //Gab
            weapon.SetActive(true);
        }

        FireProjectile();
    }

    void FireProjectile()
    {
        if (Input.GetButtonDown("Fire1") && playerShield.activeSelf)
        {

            //TODO Intantiate EnergyForce if energy bar is morethan 75%
            if (!isSpawned)
            {
                SpawnEnergyEffect();
                
            }
        }
    }
    private void FixedUpdate()
    {
        if (Input.GetButton("Fire2") && !isLocked)
        {
            if (currentHealth > 0)
            {
                StopAllCoroutines();
                //StartCoroutine(ShowEnergyBar());
            }
        }
    }

    void DeployingShieldEnergy()
    {
        if (currentHealth > 0)
        {
            if (!isSoundPlayed)
            {
                Shieldfx.PlayOneShot(shieldUsingfx);
                isSoundPlayed = true;
            }
            //currentHealth -= (currentHealth * drainingRate);
            currentHealth -= drainingRate * Time.deltaTime;
            shieldEnergyBar.fillAmount = currentHealth / maxHealth;
            if (currentHealth <= 0f)
            {
                if (isSoundPlayed)
                {
                    Shieldfx.Stop();
                    Shieldfx.PlayOneShot(shieldDepletedfx);
                }
                isLocked = true;
                EnergyBarCalmDown();
                playerShield.SetActive(false);
                shieldBeingDeployed = false;
            }
        }
    }


    void RegenerateShieldEnergy()
    { 
        if (currentHealth >= maxHealth)
        {
;           Shieldfx.Stop();
            Shieldfx.PlayOneShot(shieldReadyfx);
            maxHealth = (int)currentHealth;
            isSpawned = false;
            isLocked = false;
            //energyBarCanvasGroup.alpha = 0f;
            CancelInvoke("RegenerateShieldEnergy");
        }
        if(currentHealth < maxHealth)
        {
            if (isSoundPlayed)
            {
                isSoundPlayed = false;
                Shieldfx.Stop();
            }
            //currentHealth += (maxHealth * regenrationRate);
            currentHealth += regenrationRate * Time.deltaTime;
            shieldEnergyBar.fillAmount = currentHealth / maxHealth;
        }
    }

    //Instantiate EnergyForce
    void SpawnEnergyEffect()
    {
        Rigidbody rb = Instantiate(energyForce, firePoint.position, firePoint.rotation).GetComponent<Rigidbody>();
        rb.AddRelativeForce(Vector3.forward * appliedForceForEnergy); 
        isSpawned = true;
        //DepletTotalEnergy(currentHealth);
    }


    public void TakeEnemyDamage(float damage)
    {
        if (currentHealth > 0)
        {
            currentHealth -= damage;
            shieldEnergyBar.fillAmount = currentHealth / maxHealth;
        }
    }

    public void DepletTotalEnergy(float force) // this shit is broken so I'll set it to 100 force
    {
        if (currentHealth > 0)
        {
            if (isSoundPlayed)
            {
                Shieldfx.Stop();
                Shieldfx.PlayOneShot(shieldDepletedfx);
            }
            currentHealth = force;// -= previously
            shieldEnergyBar.fillAmount = currentHealth / maxHealth;
            isLocked = true;
            StopAllCoroutines();
            EnergyBarCalmDown();
        }
    }

    void EnergyBarCalmDown()
    {
        playerShield.SetActive(false);
        StopAllCoroutines();
        //energyBarCanvasGroup.alpha = 1f;
    }

    IEnumerator ShowEnergyBar()
    {
        playerShield.SetActive(false);
        float timer = 0f; 
        while (timer < 0.1f)
        {
            energyBarCanvasGroup.alpha = Mathf.Lerp(energyBarCanvasGroup.alpha, 1f, (timer / 0.1f));
            timer += Time.deltaTime;
            yield return null;
        }
        StartCoroutine(HideEnergyBar());
    }

    IEnumerator HideEnergyBar()
    {
        float timer = 0f;
        while (timer < 0.5f)
        {
            energyBarCanvasGroup.alpha = Mathf.Lerp(energyBarCanvasGroup.alpha, 0f, (timer / 0.5f));
            timer += Time.deltaTime;
            yield return null;
        }
    }
}
