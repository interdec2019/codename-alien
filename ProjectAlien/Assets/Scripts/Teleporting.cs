﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Teleporting : MonoBehaviour
{
    Renderer rend;

    Material[] mats;
    Material tPMat;
    Material oMat;

    
    float timeItTakesToFade = 2f;   

    private void Awake()
    {
        mats = gameObject.GetComponent<Renderer>().materials;
        oMat = mats[0];
        tPMat = mats[1];
    }

    public void ActivateTP()
    {        
        StartCoroutine(TeleportMaterialOut());
    }

    public void DectivateTP()
    {        
        StartCoroutine(TeleportMaterialIn());
    }
    public IEnumerator TeleportMaterialOut()
    {
        float _timer=0;
        rend = GetComponent<Renderer>();
        //rend.materials[0]= tPMat;
        rend.material = tPMat;
        while (_timer <= timeItTakesToFade)
        {
           
            _timer += Time.deltaTime;
            float percent = Mathf.Clamp01(_timer / timeItTakesToFade);
            rend.material.SetFloat("_Teleport", Mathf.Lerp(rend.material.GetFloat("_Teleport"), -6f, percent));

            yield return null;
        }
        StartCoroutine(TeleportMaterialIn());
        //Destroy(this);
    }
    public IEnumerator TeleportMaterialIn()
    {
        float _timer = 0;
        rend = GetComponent<Renderer>();
        
        //rend.material = tPMat;
        while (_timer <= timeItTakesToFade/3.7f)
        {            
            _timer += Time.deltaTime;
            float percent = Mathf.Clamp01(_timer / timeItTakesToFade);
            rend.material.SetFloat("_Teleport", Mathf.Lerp(rend.material.GetFloat("_Teleport"), -0.9f, percent));
            Debug.Log(_timer);
            yield return null;
        }
        rend.material.SetFloat("_Teleport", 1f);
        ResetMaterial();
       //rend.material= oMat;

        rend.material.SetFloat("_Teleport", 1f);
        //Destroy(this);
    }

    public void ResetMaterial()
    {
        StopAllCoroutines();
        rend.material = oMat;
        rend.materials[0] = oMat;
        rend.materials[1] = tPMat;
    }
}
