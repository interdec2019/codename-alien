﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableObjectPieceBehavior : MonoBehaviour
{
    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        
        rb.AddForce(new Vector3(Random.Range(500f, 1000f), Random.Range(500f, 1000f), Random.Range(500f, 1000f)));
        Destroy(gameObject, 3f);// optimizer
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
