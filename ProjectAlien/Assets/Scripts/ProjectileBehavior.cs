﻿using UnityEngine;
using System.Collections;

public class ProjectileBehavior : MonoBehaviour
{
    [SerializeField] Rigidbody projectileRB;
    GameObject projectileSpawner;

    [SerializeField] GameObject particlesImpact;

    [SerializeField] float projectileForce = 3000f;



    void Start()
    {
        projectileSpawner = GameObject.Find("ProjectileSpawner"); // we need to find the projectileSpawner in world to borrow it's Forward to direct the projectile
        transform.rotation = projectileSpawner.transform.rotation;
        projectileRB.AddRelativeForce(Vector3.forward * projectileForce); //adds force into the direction of the projectileSpawner's x axis (forward this case)

        Destroy(gameObject, 0.4f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Instantiate(particlesImpact, collision.contacts[0].point, Quaternion.Euler(collision.contacts[0].normal));
        Destroy(gameObject);

        //Bruno:- For collison of destructable objects

        Component damageable = collision.collider.GetComponent(typeof(IDamageable));
        if (damageable)
        {
            (damageable as IDamageable).TakeDamage(5f);
        }
    }



}
