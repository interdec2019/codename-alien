﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowShip : MonoBehaviour
{
    [SerializeField] GameObject ship;
    
    void Update()
    {
        transform.LookAt(ship.transform.position);
        transform.Translate(Vector3.down*0.07f);
    }
}
