// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "AndyFX/TakoDisolve"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_Texture1("Texture 1", 2D) = "white" {}
		_Distortion0("Distortion0", 2D) = "bump" {}
		_DistortionAmount0("DistortionAmount0", Range( 0 , 1)) = 1
		_Scrollspeed0("Scrollspeed0", Range( 0 , 1)) = 0
		_Colour0("Colour0", Color) = (1,0.831139,0,0)
		_Colour1("Colour1", Color) = (1,0.1535171,0,0)
		_Float5("Float 5", Range( 0.01 , 5)) = 2
		_Albedo_1("Albedo_1", 2D) = "white" {}
		_Burn0("Burn0", Range( 0 , 1.1)) = 1
		_Normal_1("Normal_1", 2D) = "bump" {}
		_Heatwave0("Heatwave0", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _Normal_1;
		uniform float4 _Normal_1_ST;
		uniform sampler2D _Albedo_1;
		uniform float4 _Albedo_1_ST;
		uniform float4 _Colour0;
		uniform float4 _Colour1;
		uniform sampler2D _Texture1;
		uniform sampler2D _Distortion0;
		uniform float4 _Distortion0_ST;
		uniform float _DistortionAmount0;
		uniform float _Scrollspeed0;
		uniform float _Float5;
		uniform float _Heatwave0;
		uniform float _Burn0;
		uniform float _Cutoff = 0.5;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Normal_1 = i.uv_texcoord * _Normal_1_ST.xy + _Normal_1_ST.zw;
			o.Normal = UnpackNormal( tex2D( _Normal_1, uv_Normal_1 ) );
			float2 uv_Albedo_1 = i.uv_texcoord * _Albedo_1_ST.xy + _Albedo_1_ST.zw;
			o.Albedo = tex2D( _Albedo_1, uv_Albedo_1 ).rgb;
			float2 uv_Distortion0 = i.uv_texcoord * _Distortion0_ST.xy + _Distortion0_ST.zw;
			float temp_output_3_0 = ( _Time.y * _Scrollspeed0 );
			float2 panner10 = ( temp_output_3_0 * float2( 0,1 ) + float2( 0,0 ));
			float2 uv_TexCoord15 = i.uv_texcoord + panner10;
			float4 lerpResult31 = lerp( _Colour0 , _Colour1 , tex2D( _Texture1, ( ( (UnpackNormal( tex2D( _Distortion0, uv_Distortion0 ) )).xyz * _DistortionAmount0 ) + float3( uv_TexCoord15 ,  0.0 ) ).xy ).r);
			float4 temp_cast_3 = (_Float5).xxxx;
			float2 panner6 = ( temp_output_3_0 * float2( 0,-1 ) + i.uv_texcoord);
			float4 tex2DNode25 = tex2D( _Texture1, ( ( (UnpackNormal( tex2D( _Distortion0, panner6 ) )).xy * _Heatwave0 ) + i.uv_texcoord ) );
			float temp_output_30_0 = step( tex2DNode25.r , _Burn0 );
			float temp_output_22_0 = ( 0.0 / 1.1 );
			float temp_output_36_0 = step( tex2DNode25.r , ( 1.0 - _Burn0 ) );
			float temp_output_41_0 = ( temp_output_36_0 - step( tex2DNode25.r , ( 1.0 - temp_output_22_0 ) ) );
			float4 temp_cast_4 = (temp_output_41_0).xxxx;
			float4 temp_cast_5 = (temp_output_41_0).xxxx;
			o.Emission = ( ( ( ( pow( lerpResult31 , temp_cast_3 ) * _Float5 ) * ( temp_output_30_0 + ( temp_output_30_0 - step( tex2DNode25.r , temp_output_22_0 ) ) ) ) - temp_cast_4 ) - temp_cast_5 ).rgb;
			o.Metallic = 0.0;
			o.Smoothness = 0.0;
			o.Alpha = temp_output_36_0;
			clip( temp_output_36_0 - _Cutoff );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float4 tSpace0 : TEXCOORD3;
				float4 tSpace1 : TEXCOORD4;
				float4 tSpace2 : TEXCOORD5;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17000
-1913;-6;1918;1017;1151.023;223.0148;1;True;False
Node;AmplifyShaderEditor.SimpleTimeNode;1;-3612.122,-309.3169;Float;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;2;-3701.053,-81.72847;Float;False;Property;_Scrollspeed0;Scrollspeed0;4;0;Create;True;0;0;False;0;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;3;-3410.053,-140.7285;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;4;-3924.451,337.3094;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;6;-3678.949,337.1153;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,-1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TexturePropertyNode;5;-3662.506,-568.5671;Float;True;Property;_Distortion0;Distortion0;2;0;Create;True;0;0;False;0;a77d49acad1169e4c81eab6c82693a46;a77d49acad1169e4c81eab6c82693a46;True;bump;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SamplerNode;8;-3356.238,-477.896;Float;True;Property;_TextureSample4;Texture Sample 4;3;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;7;-3423.592,335.5778;Float;True;Property;_TextureSample3;Texture Sample 3;12;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;12;-3082.626,431.7926;Float;False;Property;_Heatwave0;Heatwave0;11;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;10;-3197.054,-142.7262;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;13;-3004.483,-366.5197;Float;False;Property;_DistortionAmount0;DistortionAmount0;3;0;Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;11;-3083.735,335.573;Float;False;True;True;False;True;1;0;FLOAT3;0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ComponentMaskNode;9;-3052.298,-477.9184;Float;False;True;True;True;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;15;-2932.738,-138.4431;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;16;-2785.619,342.5775;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;14;-2826.92,-472.6915;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;17;-2876.177,514.4135;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;21;-2530.686,396.882;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TexturePropertyNode;18;-2519.258,-678.523;Float;True;Property;_Texture1;Texture 1;1;0;Create;True;0;0;False;0;63dca6a8e8cbd6e41aab6f08f7744281;63dca6a8e8cbd6e41aab6f08f7744281;False;white;Auto;Texture2D;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-2223.874,125.2062;Float;False;Constant;_DivideAmount0;DivideAmount0;12;0;Create;True;0;0;False;0;1.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;20;-2624.33,-378.1616;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT2;0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;26;-2326.192,22.2224;Float;False;Property;_Burn0;Burn0;9;0;Create;True;0;0;False;0;1;0;0;1.1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;22;-1975.258,56.93922;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;23;-2239.795,-679.4394;Float;True;Property;_TextureSample5;Texture Sample 5;2;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;25;-2307.613,-181.6772;Float;True;Property;_TextureSample7;Texture Sample 7;8;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;27;-1087.482,-605.2444;Float;False;Property;_Colour1;Colour1;6;0;Create;True;0;0;False;0;1,0.1535171,0,0;1,0.153517,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;24;-1090.094,-790.7993;Float;False;Property;_Colour0;Colour0;5;0;Create;True;0;0;False;0;1,0.831139,0,0;1,0.831139,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;29;-1022.833,-421.1375;Float;False;Property;_Float5;Float 5;7;0;Create;True;0;0;False;0;2;2;0.01;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;30;-1970.886,-149.2883;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;28;-1801.213,-43.98967;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;31;-750.3455,-691.4882;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;33;-1646.931,-145.1506;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;34;-1841.301,244.7278;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;35;-1837.246,464.718;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;32;-557.7319,-691.1887;Float;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.StepOpNode;38;-1577.124,454.1785;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;36;-1579.724,225.3785;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;39;-1476.406,-260.6235;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;37;-342.9192,-688.0202;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;40;-1188.801,-299.61;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;41;-1284.75,322.8788;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;42;-972.6866,129.1642;Float;True;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;45;-621.1805,-97.32076;Float;True;Property;_Albedo_1;Albedo_1;8;0;Create;True;0;0;False;0;119b0f4bfb701e445a76151c4e22ff2d;119b0f4bfb701e445a76151c4e22ff2d;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;47;-282.3356,139.1753;Float;False;Constant;_Metallic;Metallic;12;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;44;-563.1202,521.6334;Float;True;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;43;-611.3027,96.67941;Float;True;Property;_Normal_1;Normal_1;10;0;Create;True;0;0;False;0;e13f1fbdf88442843b0897cb92de6d73;e13f1fbdf88442843b0897cb92de6d73;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;48;-283.6029,210.149;Float;False;Constant;_Smoothness;Smoothness;12;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;AndyFX/TakoDisolve;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Transparent;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;3;0;1;0
WireConnection;3;1;2;0
WireConnection;6;0;4;0
WireConnection;6;1;3;0
WireConnection;8;0;5;0
WireConnection;7;0;5;0
WireConnection;7;1;6;0
WireConnection;10;1;3;0
WireConnection;11;0;7;0
WireConnection;9;0;8;0
WireConnection;15;1;10;0
WireConnection;16;0;11;0
WireConnection;16;1;12;0
WireConnection;14;0;9;0
WireConnection;14;1;13;0
WireConnection;21;0;16;0
WireConnection;21;1;17;0
WireConnection;20;0;14;0
WireConnection;20;1;15;0
WireConnection;22;1;19;0
WireConnection;23;0;18;0
WireConnection;23;1;20;0
WireConnection;25;0;18;0
WireConnection;25;1;21;0
WireConnection;30;0;25;1
WireConnection;30;1;26;0
WireConnection;28;0;25;1
WireConnection;28;1;22;0
WireConnection;31;0;24;0
WireConnection;31;1;27;0
WireConnection;31;2;23;1
WireConnection;33;0;30;0
WireConnection;33;1;28;0
WireConnection;34;0;26;0
WireConnection;35;0;22;0
WireConnection;32;0;31;0
WireConnection;32;1;29;0
WireConnection;38;0;25;1
WireConnection;38;1;35;0
WireConnection;36;0;25;1
WireConnection;36;1;34;0
WireConnection;39;0;30;0
WireConnection;39;1;33;0
WireConnection;37;0;32;0
WireConnection;37;1;29;0
WireConnection;40;0;37;0
WireConnection;40;1;39;0
WireConnection;41;0;36;0
WireConnection;41;1;38;0
WireConnection;42;0;40;0
WireConnection;42;1;41;0
WireConnection;44;0;42;0
WireConnection;44;1;41;0
WireConnection;0;0;45;0
WireConnection;0;1;43;0
WireConnection;0;2;44;0
WireConnection;0;3;47;0
WireConnection;0;4;48;0
WireConnection;0;9;36;0
WireConnection;0;10;36;0
ASEEND*/
//CHKSM=3582947C58FD5334B6AC9352C726292087E44CC3